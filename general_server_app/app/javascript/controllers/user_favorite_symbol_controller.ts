import { Controller } from "stimulus"
import ToggleFavorites from '@/views/toggle_favorites.vue'
import Vue from 'vue'

export default class extends Controller {
  connect(): void {
    const app = new Vue({
      el: this.element,
      render: h => h(ToggleFavorites),
      data: {
        symbol: this.data.get('symbolSlug')
      }
    })
  }
}
