import { Controller } from "stimulus"
import axios, {AxiosResponse} from 'axios'
import routes from '@/utilities/routes'

export default class extends Controller {
  menuButtonTarget: Element
  menuContainerTarget: Element
  logoutButtonTarget: Element

  static targets = [ 'menuContainer', 'menuButton', 'logoutButton' ]

  connect(): void {
    this.toggleMenu = this.toggleMenu.bind(this)
    this.menuButtonTarget.addEventListener('click', this.toggleMenu)
    this.logoutButtonTarget.addEventListener('click', this.logout)
  }

  toggleMenu (): void {
    this.menuButtonTarget.classList.toggle('is-active')
    this.menuContainerTarget.classList.toggle('is-active')
  }

  async logout () {
    const token = document.querySelector("meta[name='csrf-token']") && document.querySelector("meta[name='csrf-token']").getAttribute('content')
    const queryParams = { data: { authenticity_token: token }}

    const result: AxiosResponse<{ success: boolean }> = await axios.delete(routes.users.logout, queryParams)

    if (result.data.success) {
      window.location.href = '/'
    }
  }

  disconnect(): void {
    this.menuButtonTarget.removeEventListener('click',this.toggleMenu)
  }
}
