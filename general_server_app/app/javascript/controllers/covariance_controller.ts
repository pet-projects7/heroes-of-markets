import { Controller } from "stimulus"
import Covariance from '@/views/covariance.vue'
import Vue from 'vue'
import Autocomplete from 'v-autocomplete'

Vue.use(Autocomplete)

// import component and stylesheet
import AirbnbStyleDatepicker from 'vue-airbnb-style-datepicker'
import 'vue-airbnb-style-datepicker/dist/vue-airbnb-style-datepicker.min.css'

// see docs for available options
const datepickerOptions = {}

// make sure we can use it in our components
Vue.use(AirbnbStyleDatepicker, datepickerOptions)

export default class extends Controller {
  connect(): void {
    const app = new Vue({
      el: this.element,
      render: h => h(Covariance)
    })
  }
}
