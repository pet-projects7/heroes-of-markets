import { Controller } from "stimulus"
import SearchStockSymbols from '@/views/search_stock_symbols.vue'
import Vue from 'vue'

export default class extends Controller {
  connect(): void {
    const app = new Vue({
      el: this.element,
      render: h => h(SearchStockSymbols),
      data: {
        market: this.data.get('market')
      }
    })
  }
}
