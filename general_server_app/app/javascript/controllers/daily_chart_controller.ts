import { Controller } from "stimulus"
import DailyChart from '@/views/daily_chart.vue'
import Vue from 'vue'

export default class extends Controller {
  connect(): void {
    const app = new Vue({
      el: this.element,
      render: h => h(DailyChart),
      data: {
        stockSymbolTitle: this.data.get('stockSymbolTitle'),
        stockSymbol: parseInt(this.data.get('stockSymbol'))
      }
    })
  }
}
