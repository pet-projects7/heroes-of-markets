import { Controller } from "stimulus"
import FavoritedStocksList from '@/views/favorited_stocks_list.vue'
import Vue from 'vue'

export default class extends Controller {
  connect(): void {
    const app = new Vue({
      el: this.element,
      render: h => h(FavoritedStocksList)
    })
  }
}
