import { Controller } from "stimulus"
import { scaleLinear } from "d3-scale"
import { color, rgb } from "d3-color";

export default class extends Controller {

  connect(): void {
    new AnnualSummary({
      tableCells: this.element.querySelectorAll('[data-type="month"]')
    })

    new AnnualSummary({
      tableCells: this.element.querySelectorAll('[data-type="year"]')
    })
  }
}

class AnnualSummary {
  private tableCells;
  private monthValues;
  private monthMinimum;
  private monthMaximum;
  private monthScale;

  constructor (options) {
    this.tableCells = options.tableCells

    this.setMonthValues(this.tableCells)
    this.setMonthExtremus(this.monthValues)
    this.setMonthScale(this.monthMinimum, this.monthMaximum)
    this.colorMonthCells()
  }

  private setMonthValues (tableCells) {
    this.monthValues = Array.from(tableCells)
      .map((cell: HTMLBaseElement) => {
        if (!cell.dataset.absolute) {
          return null
        }

        return { absolute: Number(cell.dataset.absolute), percent: Number(cell.dataset.percent) }
      })
      .filter( (monthValue) => monthValue != null )
  }

  private overlayColor (color) {
    if(color.length < 5) {
      color += color.slice(1);
    }
    return (color.replace('#','0x')) > (0xffffff/2) ? '#333' : '#fff';
  };

  private colorMonthCells () {
    this.tableCells.forEach((cell: HTMLBaseElement) => {
      if (!cell.dataset.absolute) {
        return
      }

      const backgroundColor = rgb(this.monthScale(cell.dataset.absolute)).hex()
      cell.style.background = backgroundColor
      cell.style.color = this.overlayColor(backgroundColor)
    })
  }

  private setMonthScale (minimumValue, maximumValue) {
    this.monthScale = scaleLinear<d3.RGBColor | d3.HSLColor>()
      .domain([minimumValue, 0, maximumValue])
      .range([
        color('red'),
        color('#c7c7c7'),
        color('green')
      ])
  }

  private setMonthExtremus (values) {
    const absoluteValues = values.map(monthValue => monthValue.absolute)
    const orderedMonthValues = absoluteValues.sort(function (monthOneValue, monthTwoValue) {
      if (monthOneValue < monthTwoValue) {
        return -1
      }

      if (monthOneValue > monthTwoValue) {
        return 1
      }

      return 0
    })

    this.monthMinimum = orderedMonthValues[0]
    this.monthMaximum = orderedMonthValues[orderedMonthValues.length - 1]
  }
}
