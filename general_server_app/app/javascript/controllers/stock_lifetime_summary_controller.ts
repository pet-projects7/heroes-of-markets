import { Controller } from "stimulus"
import StockLifetimeSummary from '@/views/stock_lifetime_summary.vue'
import Vue from 'vue'

export default class extends Controller {
  static targets = [ "output" ]

  connect(): void {
    const app = new Vue({
      el: this.element,
      render: h => h(StockLifetimeSummary),
      data: {
        stockSymbolTitle: this.data.get('stockSymbolTitle'),
        stockSymbol: parseInt(this.data.get('stockSymbol')),
        marketID: parseInt(this.data.get('marketId')),
      }
    })
  }
}
