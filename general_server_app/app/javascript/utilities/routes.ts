export default {
  search_market_stock_symbols (market_id: number): string {
    return `/markets/${ market_id }/stock_symbols/search.json/`
  },
  search_stock_symbols (): string {
    return `/stock_symbols/search.json/`
  },
  market_stock_symbol (market_id: number | string, stock_symbol_id: string): string {
    return `/markets/${ market_id }/stock_symbols/${ stock_symbol_id }/`
  },
  stock_symbol_daily_summaries (market_id: number, stock_symbol_id: number): string {
    return `/markets/${ market_id }/stock_symbols/${ stock_symbol_id }/daily_stock_summaries/`
  },
  users: {
    logout: '/users/logout'
  },
  api: {
    stock_symbol_daily_summaries (stock_symbol: string): string {
      return `/api/v1/stock_symbols/${ stock_symbol }/daily_stock_summaries/`
    },
    stock_symbol_short_summary (stock_symbol: string): string {
      return `/api/v1/stock_symbols/${ stock_symbol }/short_summary/`
    },
    favorites: {
      check (symbol_id: string) {
        return `/api/v1/stock_symbols/${ symbol_id }/user_favorite_symbols/check/`
      },
      toggle (symbol_id: string) {
        return `/api/v1/stock_symbols/${ symbol_id }/user_favorite_symbols/toggle/`
      }
    },
    intraday_stock_market_data: '/api/v1/intraday_stock_market_data',
    user: {
      favorited_stocks: '/api/v1/user_favorite_symbols'
    }
  }
}
