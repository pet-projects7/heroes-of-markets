import * as d3 from "d3";

export class RangesManagement {
  static availableRanges = {
    1: {
      label: "Day",
      ticks: d3.timeHour.every(1),
      ticksFormat: d3.timeFormat("%H:%M"),
      legendFormat(date) {
        return d3.timeFormat("%B %d %Y");
      },
      range: 1,
      interval: 2,
      values: null,
      active: false,
      loading: true
    },
    7: {
      label: "Week",
      ticks: 3,
      interval: 15,
      range: 7,
      ticksFormat: d3.timeFormat("%b %d (%a)"),
      legendFormat: d3.timeFormat("%B %d %Y"),
      values: null,
      active: false,
      loading: false
    },
    30: {
      label: "Month",
      ticks: 5,
      interval: 60,
      ticksFormat: d3.timeFormat("%b %d (%a)"),
      legendFormat: d3.timeFormat("%B %d %Y"),
      range: 30,
      values: null,
      active: false,
      loading: false
    },
    90: {
      label: "3 monthes",
      ticks: d3.timeMonth.every(1),
      ticksFormat: d3.timeFormat("%B %d"),
      legendFormat: d3.timeFormat("%b %d %Y"),
      range: 90,
      values: null,
      active: false,
      loading: false
    },
    365: {
      label: "1 year",
      ticks: d3.timeMonth.every(3),
      ticksFormat: d3.timeFormat("%b"),
      legendFormat: d3.timeFormat("%B %d %Y"),
      range: 365,
      values: null,
      active: false,
      loading: false
    },
    all: {
      label: "Whole history",
      ticks: d3.timeYear.every(1),
      ticksFormat: d3.timeFormat("%Y"),
      legendFormat: d3.timeFormat("%B %d %Y"),
      range: "all",
      values: null,
      active: false,
      loading: false
    }
  }
}
