export namespace WorldTradingAPI {
  export interface DailySummary {
    close: string,
    date: string,
    high: number,
    id: number,
    low: number,
    open: string,
    stock_symbol_id: number,
    updated_at: string,
    volume: number
  }

  export interface IIntradayStockData {
    symbol: string,
    stock_exchange_short: string,
    timezone_name: string,
    intraday: IIntradayStockDataItem
  }

  export interface IIntradayStockDataItem {
    [key: string]: { close: string, high: string, low: string, open: string, volume: string }
  }

  export interface IShortSummary {
    "symbol": string,
    "name": string,
    "currency": string,
    "price": string,
    "price_open": string,
    "day_high": string,
    "day_low": string,
    "52_week_high": string,
    "52_week_low": string,
    "day_change": string,
    "change_pct": string,
    "close_yesterday": string,
    "market_cap": string,
    "volume": string,
    "volume_avg": string,
    "shares": string,
    "stock_exchange_long": string,
    "stock_exchange_short": string,
    "timezone": string,
    "timezone_name": string,
    "gmt_offset": string,
    "last_trade_time": string
  }
}
