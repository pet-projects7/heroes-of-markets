export namespace API {
  export interface IStockSymbol {
    id: number,
    title: string,
    market_id: number,
    currency: string,
    full_name: string,
    slug: string,
    market?: IMarket
  }

  export interface IMarket {
    title: number,
    description: string,
    symbol: string,
    site: string,
    slug: string,
    state: string
  }

  export interface IPaginationInfo {
    count: number
    from: number
    items: number
    last: number
    next: number
    offset: number
    outset: number
    page: number
    pages: number
    prev: number | null
    to: number
  }

  export interface IUserFavoritedStock {
    id: number,
    type: string,
    relationships: string,
    links: {
      stock_symbol: IStockSymbol,
      market: IMarket
    }
  }
}
