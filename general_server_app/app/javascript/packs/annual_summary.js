import { Controller } from "stimulus"
import { Application } from "stimulus"

export default class AnnualSummaryController extends Controller {
  connect() {
    console.log("Hello, Stimulus!", this.element)
  }

  greet() {
  }
}

const application = Application.start()
application.register("annual_summary", AnnualSummaryController)
