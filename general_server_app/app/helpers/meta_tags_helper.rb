# frozen_string_literal: true

module MetaTagsHelper
  def heroes_of_markets_meta_tags
    path = "#{controller_path}_#{action_name}".tr('/', '_')

    respond_to?(path.to_sym, true) ? send(path.to_sym) : default_tags
  end

  private

  def default_tags
    {
      site: 'HOM',
      title: 'Heroes of Markets',
      description:
        'List of worldwide markets. Where you can follow last news and find all kind of charts.',
      keywords: 'stocks, markets, news, analytics, nasdaq, nyse',
      charset: 'utf-8',
      image_src: asset_pack_path('media/images/facebook.jpg'),
      og: {
        title: :title,
        site_name: :site,
        type: 'website',
        image: :image_src,
        'image:width': '1080px',
        'image:height': '1080px'
      },
      icon: [
        {
          rel: 'apple-touch-icon',
          sizes: '180x180',
          href: asset_pack_path('media/images/favicons/apple-touch-icon.png')
        },
        {
          rel: 'icon',
          type: 'image/png',
          sizes: '32x32',
          href: asset_pack_path('media/images/favicons/favicon-32x32.png')
        },
        {
          rel: 'icon',
          type: 'image/png',
          sizes: '16x16',
          href: asset_pack_path('media/images/favicons/favicon-16x16.png')
        },
        {
          rel: 'mask-icon',
          href: asset_pack_path('media/images/favicons/safari-pinned-tab.svg'),
          color: '#5bbad5'
        }
      ],
      reverse: true
    }
  end

  def markets_index
    default_tags
  end

  def stock_symbols_index
    new_tags = { noindex: true, nofollow: true }

    default_tags.update new_tags
  end

  def markets_show
    title = @market.title.presence || @market.symbol

    new_tags = {
      title: "Stocks exchange market: #{title}",
      description:
        "Personal page of stocks exchange market: #{
          title
        }. With information about all stocks on this market. Everyday analytics of market leaders and loosers. News and opinions",
      keywords: "#{title} market, market stocks"
    }

    default_tags.update new_tags
  end

  def markets_stock_symbols_show
    market_title = @market.title
    stock_symbol_title = @symbol.full_name

    new_tags = {
      title:
        "Stock symbol #{stock_symbol_title} on stock exchange market #{
          market_title
        }",
      description:
        "Annual summary and all history statistics of #{stock_symbol_title}",
      keywords: stock_symbol_title.to_s
    }

    default_tags.update new_tags
  end

  def users_new
    new_tags = {
      title: 'Sign up',
      description:
        'Registerging giving you posibilitiy to add stocks to list of your favorites'
    }

    default_tags.update new_tags
  end

  def users_login
    new_tags = { title: 'Sign in', description: 'Sign in to your account' }

    default_tags.update new_tags
  end

  def tools_index
    new_tags = {
      title: 'Corporate finance tools',
      description: 'Tools that helps calculate some financial statistics'
    }

    default_tags.update new_tags
  end

  def tools_covariance
    new_tags = {
      title: 'Covariance',
      description:
        'Covariance is a measure of the relationship between two random variables'
    }

    default_tags.update new_tags
  end
end
