# frozen_string_literal: true

module ApplicationHelper
  include Pagy::Frontend

  def market_flag(market_symbol_upcased)
    if [ "AEX", "AMEX", "ASX", "BCBA", "BIST", "BOM", "BSE", "CNQ", "DOH",
         "FRA", "GER", "HKEX", "JSE", "JSX", "KLS", "KSC", "LSE", "MAD",
         "MCX", "MEX", "MIL", "NASDAQ", "NSE", "NYSE", "NZX", "OMX", "OMXC",
         "OMXH", "OMXT", "PAR", "SAO", "SET", "SIX", "SZCE",
         "TADAWUL", "TASE", "TSX", "TWSE", "USA", "VSE", "WSE" ].include?(market_symbol_upcased)
      image_pack_tag "media/images/flags/#{ market_symbol_upcased }.svg", \
        class: "itemsList__flag", \
        title: "Logotype of #{ market_symbol_upcased } market"
    end
  end
end
