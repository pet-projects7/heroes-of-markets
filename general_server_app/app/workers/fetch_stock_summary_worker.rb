# frozen_string_literal: true

class FetchStockSummaryWorker
  include Sidekiq::Worker
  sidekiq_options queue: :symbols_summary_crawler, retry: false, backtrace: true
  attr_accessor :summary

  def initialize
    super
    self.summary = { years: {}, highest: 0, lowest: nil, first: nil, last: nil }
  end

  def perform(stock_id)
    stock = get_stock(stock_id)
    history_data = fetch_history_data(stock)

    return if history_data.nil?

    years = history_data.group_by { |summary| summary[:date].year }

    years.each do |year, values|
      summary[:years][year] = {}
      summary[:years][year][:monthes] = {}

      monthes = values.group_by { |summary| summary[:date].month }
      monthes.each { |month, values| dig_month_result(month, values, year) }

      summary[:years][year][:result] =
        dig_year_result(summary[:years][year][:monthes])
    end

    dig_total_result(history_data)
    dig_first_price(history_data)
    dig_last_price(history_data)
    lowest(history_data)
    highest(history_data)

    stock.update(lifetime_summary: summary)
    stock.save
  end

  private

  def dig_first_price(history_data)
    history_data.reverse.each do |date_data|
      if date_data[:open].to_f > 0
        break summary[:first] = {
          value: date_data[:open], date: date_data[:date]
        }
      end
    end
  end

  def dig_last_price(history_data)
    summary[:last] = {
      date: history_data.first[:date], value: history_data.first[:close]
    }
  end

  def fetch_history_data(stock)
    query = { symbol: stock.title }

    worker = HistoryWorldTradingData.call query

    return nil if worker.response.count.zero?

    worker.response
  end

  def dig_total_result(history_data)
    summary[:total_result] =
      (history_data.first[:close].to_f - history_data.last[:open].to_f).round(2)
  end

  def dig_month_result(month, values, year)
    first_month_day = values.last
    last_month_day = values.first

    summary[:years][year][:monthes][month] = {}
    summary[:years][year][:monthes][month][:absolute] =
      (last_month_day[:close].to_f - first_month_day[:open].to_f).round(2)
    summary[:years][year][:monthes][month][:percent] =
      (
        (summary[:years][year][:monthes][month][:absolute] * 100) /
          first_month_day[:open].to_f
      )
        .round(2)
  end

  def lowest(history_data)
    summary[:lowest] =
      history_data.filter { |day| !day[:low].to_f.zero? }.sort do |a, b|
        a[:low].to_f <=> b[:low].to_f
      end.first[
        :low
      ]
        .to_f
  end

  def highest(history_data)
    summary[:highest] =
      history_data.sort { |a, b| a[:high].to_f <=> b[:high].to_f }.last[:high]
        .to_f
  end

  def dig_year_result(monthes_values)
    {
      absolute: monthes_values.values.pluck(:absolute).sum,
      percent: monthes_values.values.pluck(:percent).sum
    }
  end

  def get_stock(stock_id)
    StockSymbol.find(stock_id)
  end
end
