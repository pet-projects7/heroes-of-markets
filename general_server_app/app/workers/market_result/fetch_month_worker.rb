class MarketResult::FetchMonthWorker
  include Sidekiq::Worker

  def perform(marketID)
    # Первый год работы биржы
    market = Market.find(marketID)
    dates_array = array_of_month_year(market)
    results = []

    dates_array.each do |year_month_num|
      year, month_num = year_month_num

      leaders = results_by_month(market.stock_symbols,
                                 year,
                                 month_num,
                                 true)
      non_leaders = results_by_month(market.stock_symbols,
                                     year,
                                     month_num,
                                     false)

      results += leaders.map { |leader| prepare_object(:leaders, year, month_num, leader,  marketID, leader.id) }

      results += non_leaders.map { |leader| prepare_object(:loosers, year, month_num, leader,  marketID, leader.id) }
    end

    MarketResult.import results
  rescue NoMethodError
    p 'Нет lifetime_summary'
  end

  private

  def prepare_object(category, year, month_num, leader, market_id, stock_symbol_id)
    {
      market_id: market_id,
      stock_symbol_id: stock_symbol_id,
      category: category,
      year: year,
      month: month_num,
      percent_result: leader.data_for_month['percent'],
      absolute_result: leader.data_for_month['absolute'],
    }
  rescue TypeError

  end

  def results_by_month(stock_symbols, year, month, leaders = true)
    order_direction = leaders ? 'DESC' : 'ASC'

    stock_symbols
      .with_lifetime_summary
      .where(Arel.sql("(lifetime_summary->'years'->'#{ year }'->'monthes'->'#{ month }') is not null"))
      .select(Arel.sql("lifetime_summary->'years'->'#{ year }'->'monthes'->'#{ month }' as data_for_month"), :id)
      .order(Arel.sql("lifetime_summary->'years'->'#{ year }'->'monthes'->'#{ month }' #{ order_direction }"))
      .limit(5)
  end

  def array_of_month_year(market, year = nil)
    return current_year_monthes(year) if year

    first_year = market.first_year
    year_month_num = []

    (first_year..DateTime.now.year).to_a.each do |year|
      (1..12).to_a.each do |month_num|
        year_month_num += [[year, month_num]]
      end
    end

    year_month_num
  end

  def current_year_monthes(selected_year)
    year_month_num = []

    (DateTime.now.year..DateTime.now.year).to_a.each do |year|
      (1..12).to_a.each do |month_num|
        year_month_num += [[year, month_num]]
      end
    end

    year_month_num
  end
end
