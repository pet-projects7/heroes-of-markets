class CollectStocksDataWorker
  include Sidekiq::Worker

  def perform(*args)
    batch = Sidekiq::Batch.new
    batch.description = "Загрузка кратких итогов работы"
    batch.on(:complete, 'CollectStocksDataWorker#on_success', batch_id: batch.bid)

    Telegram::SendMessage.new.call 'Started gathering summary about stocks'

    batch.jobs do
      # Market.find(427).stock_symbols.find_in_batches batch_size: 1000 do |stocks|
      StockSymbol.find_in_batches batch_size: 1000 do |stocks|
        stocks.each { |stock| FetchStockSummaryWorker.perform_async(stock.id) }
      end
    end
  end

  def on_success(status, options)
    Telegram::SendMessage.new.call "Finished gathering summary about stocks, total stocks is #{status.total}, and failures with #{status.failures}. Batch id is #{options['batch_id']}"

    Telegram::SendMessage.new.call 'Start refreshing market results'

    MarketResult.delete_all

    # Market.where(slug: 'mcx').each do |market|
    Market.all.each do |market|
      MarketResult::FetchMonthWorker.new.perform(market.id)
    end

    Telegram::SendMessage.new.call 'Finish refreshing market results'
  end
end
