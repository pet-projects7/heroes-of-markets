# frozen_string_literal: true

class ParseSymbolHistoryWorker
  include Sidekiq::Worker
  sidekiq_options queue: :symbols_summary_crawler, retry: false, backtrace: true

  def perform(options)
    query = { symbol: options['symbol'] || options[:symbol] }
    date_from = options['date_from'] || options[:date_from]
    query[:date_from] = date_from if date_from.present?

    HistoryWorldTradingData.call query
  end
end
