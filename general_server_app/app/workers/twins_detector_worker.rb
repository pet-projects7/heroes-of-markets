# frozen_string_literal: true

class TwinsDetectorWorker
  include Sidekiq::Worker
  attr_accessor :twins, :originates

  def perform(*_args)
    possible_twins.each do |twin_stock|
      title_without_dot = twin_stock.title.gsub /\.+.+/, ''
      possible_parent = originate_stocks.find_by title: title_without_dot

      twin_stock.update(parent_stock: possible_parent) if possible_parent.present?
    end
  end

  private

  def originate_stocks
    originates ||= StockSymbol.where.not 'title LIKE ?', '%.%'
  end

  def possible_twins
    twins ||= StockSymbol.where 'title LIKE ?', '%.%'
  end
end
