# frozen_string_literal: true

class GetMarketStatusWorker
  include Sidekiq::Worker

  def perform(*_args)
    stocks =
      StockSymbol.where(title: Settings.markets.to_h.values.map(&:upcase))
    stocks_data = ShortSummary.call(symbol: stocks.pluck(:title))

    stocks_data.response.each do |stock_with_data|
      market = stocks.find_by(title: stock_with_data['symbol']).market
      toggle_market(stock_with_data, market)
    end
  end

  private

  def toggle_market(stock, market)
    last_trade_date = Date.parse stock['last_trade_time']
    today = Time.zone.today

    last_trade_datetime = DateTime.parse stock['last_trade_time']
    today_time = DateTime.now

    if last_trade_date == today && market.working_status != :opened
      market.update(working_status: :opened)
      Telegram::SendMessage.new.call "#{market.title} is opened"
    elsif market.working_status != :closed && today_time > last_trade_datetime
      market.update(working_status: :closed)
      Telegram::SendMessage.new.call "#{market.title} is closed"
    end
  end
end
