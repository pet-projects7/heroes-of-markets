# frozen_string_literal: true

class UserEmailConfirmationMailer < ApplicationMailer
  def confirm(user)
    user_email = user.email
    @token = JwtService.encode(user: user_email, exp: 1.day.from_now.to_i)
    mail(to: user_email, subject: 'Please confirm your email')
  end
end
