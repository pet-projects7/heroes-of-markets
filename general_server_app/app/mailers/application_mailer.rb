# frozen_string_literal: true

class ApplicationMailer < ActionMailer::Base
  default from: 'no-reply@heroes-of-markets.com'
  layout 'mailer'
end
