class DailyStocksSummariesCallback < BaseSidekiqCallback
  def on_complete(status, options)
    Telegram::SendMessage.new.call "Finished gathering summary about stocks, total stocks is #{status.total}, and failures with #{status.failures}. Batch id is #{options['batch_id']}"

    Telegram::SendMessage.new.call 'Start refreshing market results'

    MarketResult.delete_all

    # Market.where(slug: 'mcx').each do |market|
    Market.all.each do |market|
      MarketResult::FetchMonthWorker.new.perform(market.id)
    end

    Telegram::SendMessage.new.call 'Finish refreshing market results'
  end
end
