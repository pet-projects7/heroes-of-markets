class BaseSidekiqCallback
  def on_complete(status, options)
    puts "Uh oh, batch has failures" if status.failures != 0
  end

  def on_success(status, options)
    puts "#{options['uid']}'s batch succeeded.  Kudos!"
  end

  def on_death
  end
end
