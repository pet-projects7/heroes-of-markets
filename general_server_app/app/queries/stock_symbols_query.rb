# frozen_string_literal: true

# https://github.com/infinum/rails-handbook/blob/master/Design%20Patterns/Query%20Objects.md

class StockSymbolsQuery
  attr_reader :relation

  def initialize(relation = StockSymbol.all)
    @relation = relation
  end

  def search(params)
    search_query = params[:search_query]

    if search_query.present?
      @relation.where(
        'title ILIKE ? OR full_name ILIKE ?',
        "%#{search_query}%",
        "%#{search_query}%"
      )
    else
      @relation
    end
  end

  private

  def custom_sql; end
end
