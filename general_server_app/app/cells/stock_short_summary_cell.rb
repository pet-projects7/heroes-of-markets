# frozen_string_literal: true

class StockShortSummaryCell < Cell::ViewModel
  def show
    render
  end

  private

  def lifetime_summary
    model.lifetime_summary
  end

  def currency
    model.currency
  end
end
