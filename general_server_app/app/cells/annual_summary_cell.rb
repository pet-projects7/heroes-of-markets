# frozen_string_literal: true

class AnnualSummaryCell < Cell::ViewModel
  include Webpacker::Helper
  include Cell::Slim

  def show
    render
  end

  def index
    render
  end

  private

  def years_keys
    years = model && model['years'] ? model['years'].keys : []

    years
  end

  def monthes
    (1..12).to_a
  end

  def loaded?
    model['years'][years_keys.first]['result'].is_a?(Object)
  end

  def month_names
    Date::MONTHNAMES.slice(1..-1).map(&:to_sym)
  end

  def abbr_month_names
    Date::ABBR_MONTHNAMES.slice(1..-1).map(&:to_sym)
  end

  def value_for_month(year, month)
    model['years'][year]['monthes'][month.to_s] || ''
  end

  def value_for_year(year)
    if model['years'][year]['result'].is_a?(Object)
      model['years'][year]['result']
    else
      {}
    end
  end
end
