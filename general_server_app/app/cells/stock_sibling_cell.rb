# frozen_string_literal: true

class StockSiblingCell < Cell::ViewModel
  def show
    render
  end
end
