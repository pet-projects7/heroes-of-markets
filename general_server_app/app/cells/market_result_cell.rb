class MarketResultCell < Cell::ViewModel
  attr_reader :cached_results,
              :cached_results_for_all_years
  attr_accessor :cached_monthes_names

  def show
    render
  end

  def index
    render
  end

  def results
    self.cached_results ||= @model.market_results.where(year: year).includes(:stock_symbol)
  end

  def results_for_all_years
    self.cached_results_for_all_years ||= @model.market_results.includes(:stock_symbol)
  end

  def leaders(month)
    month.select {|market_result| market_result.category == 'leaders'}
  end

  def loosers(month)
    month.select {|market_result| market_result.category == 'loosers'}
  end

  def title(market_result)
    "#{ market_result.stock_symbol.title }"

    # "#{ market_result.stock_symbol.full_name }. Absolute result is #{ market_result.absolute_result } #{ market_result.stock_symbol.currency } and percent result is #{ market_result.percent_result }"
  end

  def result_title(market_result)
    "#{ market_result.absolute_result } #{ market_result.stock_symbol.currency } | #{ market_result.percent_result }%"
  end

  def current_year
    DateTime.now.year
  end

  def monthes_names
    self.cached_monthes_names ||= I18n.t("date.month_names").slice(1...)
  end

  def available_years
    self.cached_results_for_all_years.keys
  end

  def monthes_by_year(year)
    self.cached_results_for_all_years[year].group_by {|val| val.month}
  end

  private

  def year
    @options[:year] || DateTime.now.year
  end

  def cached_results=(value)
    @cached_results = value.to_a.group_by {|val| val.month}
  end

  def cached_results_for_all_years=(value)
    @cached_results_for_all_years = value.to_a.group_by {|val| val.year}
  end
end
