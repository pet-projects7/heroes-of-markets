# frozen_string_literal: true

class ToolsController < ApplicationController
  skip_before_action :authorize_user

  def index; end

  def covariance; end
end
