# frozen_string_literal: true

class Panel::PanelController < PanelController
  def index; end
end
