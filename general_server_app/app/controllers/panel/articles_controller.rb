# frozen_string_literal: true

class Panel::ArticlesController < PanelController
  include Pagy::Backend

  def index
    @pagy, @articles = pagy(Article)
  end

  def new
    @article = Article.new
  end

  def edit
    @article = Article.find(params[:id])
    @article.article_stock_symbol_effects.build
  end

  def update
    @article = Article.find(params[:id])

    if @article.update prepared_article_params
      redirect_to panel_articles_path(@article)
    else
      render :edit
    end
  end

  def create
    @article = Article.new article_params

    if @article.save
      redirect_to edit_panel_article_path(@article)
    else
      render :new
    end
  end

  private

  def article_params
    params.require(:article).permit(
      :title, :content, :source_link, :source_description,
      article_stock_symbol_effects_attributes: %i[stock_symbol_id effect id]
    )
  end

  def prepared_article_params
    local_params = article_params.to_h

    local_params[:article_stock_symbol_effects_attributes] = local_params[:article_stock_symbol_effects_attributes].values.filter { |assea| assea['effect'].present? && assea['stock_symbol_id'].present? }

    local_params
  end
end
