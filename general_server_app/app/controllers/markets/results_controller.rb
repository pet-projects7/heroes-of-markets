# frozen_string_literal: true

module Markets
  class ResultsController < ApplicationController
    skip_before_action :authorize_user
    before_action :find_market

    def index; end

    private

    def find_market
      @market = Market.friendly.find(params[:market_id]).decorate
    end
  end
end
