# frozen_string_literal: true

module Markets
  class StockSymbolsController < ApplicationController
    before_action :find_market, only: %i[search index show]
    before_action :find_symbol, only: %i[show]
    skip_before_action :authorize_user

    include Pagy::Backend

    def index
      @pagy, @records = pagy(StockSymbol)
    end

    def show
      redirect_to [@market, @symbol], status: :moved_permanently if /^\d+$/ =~ params[:id] && @symbol.slug.present?

      # respond_to do |format|
      #   format.html do; end
      #   format.json do
      #     render json: @symbol.stocks
      #   end
      # end
    end

    def search
      search_results =
        StockSymbolsQuery.new(@market.stock_symbols).search(search_params)

      @pagy, @records = pagy(search_results, items: 100)
      render json: { data: @records.decorate, meta: @pagy }
    end

    private

    def find_symbol
      @symbol = @market.stock_symbols.friendly.find(params[:id]).decorate

      if /\d+/.match?(params[:id]) do
           redirect_to market_stock_symbol_path(
             params[:market_id],
             @symbol.slug
           ), status: :moved_permanently
         end
      end
    end

    def find_market
      @market = Market.friendly.find(params[:market_id]).decorate
    end

    def search_params
      params.permit :search_query, :page
    end
  end
end
