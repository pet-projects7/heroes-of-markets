# frozen_string_literal: true

# Контроллер для корневого роута /stocks
class StockSymbolsController < ApplicationController
  skip_before_action :authorize_user

  include Pagy::Backend

  def index
    @pagy, @records = pagy(StockSymbol)
  end

  def search
    search_results =
      StockSymbolsQuery.new(StockSymbol.includes(:market)).search(search_params)

    @pagy, @records = pagy(search_results, items: 100)
    render json: {
      data: @records.decorate.to_json(include: :market), meta: @pagy
    }
  end

  private

  def search_params
    params.permit :search_query, :page
  end
end
