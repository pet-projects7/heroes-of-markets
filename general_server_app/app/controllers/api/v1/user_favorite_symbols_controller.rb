# frozen_string_literal: true

module Api
  module V1
    class UserFavoriteSymbolsController < ApplicationController
      def toggle
        if params[:setStatus]
          @current_user.user_favorite_symbols.create(
            stock_symbol: StockSymbol.find_by(slug: params[:stock_symbol_id])
          )
        else
          @current_user.user_favorite_symbols.includes(:stock_symbol).where(
            stock_symbols: { slug: params[:stock_symbol_id] }
          )
            &.first
            &.delete
        end

        render json: { data: 'success' }
      end

      def check
        favorite_symbol =
          @current_user.stock_symbols.find_by(slug: params[:stock_symbol_id])

        render json: { data: { favorited: favorite_symbol.present? } }
      end

      def index
        @user_stocks =
          @current_user.user_favorite_symbols.includes(stock_symbol: %i[market])
        render json:
                 UserFavoriteStockSerializer.new(@user_stocks).serialized_json
      end
    end
  end
end
