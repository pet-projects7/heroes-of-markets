# frozen_string_literal: true

module Api
  module V1
    class DailyStockSummariesController < ApplicationController
      skip_before_action :authorize_user

      def index
        summaries = HistoryWorldTradingData.new(query_params).get_json

        render json: { data: summaries }
      end

      private

      def find_stock_symbol
        @stock_symbol = StockSymbol.find(params[:stock_symbol_id])
      end

      def query_params
        local_params = {}
        local_params[:symbol] = params[:stock_symbol_id] if params[:stock_symbol_id]
        local_params[:date_from] = params[:date_from] if params[:date_from]

        local_params
      end
    end
  end
end
