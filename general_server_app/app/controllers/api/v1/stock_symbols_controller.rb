# frozen_string_literal: true

module Api
  module V1
    class StockSymbolsController < ApplicationController
      skip_before_action :authorize_user

      def short_summary
        data = ShortSummary.call(symbol: [params['id']])
        render json: { data: data.response[0] }
      end
    end
  end
end
