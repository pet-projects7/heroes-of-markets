# frozen_string_literal: true

module Api
  module V1
    class IntradayStockMarketDataController < ApplicationController
      skip_before_action :authorize_user

      def show
        request_params = {
          symbol: params[:symbol],
          range: params[:range],
          interval: params[:interval]
        }

        render json: FetchIntradayStockMarketData.new.call(request_params)
      rescue ArgumentError
        render json: { errors: 'incorrect params' }
      end

      def intraday_params
        params.permit(:symbol, :interval, :range)
      end
    end
  end
end
