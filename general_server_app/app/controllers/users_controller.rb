# frozen_string_literal: true

class UsersController < ApplicationController
  skip_before_action :authorize_user, except: %i[profile favorited]

  def create
    user_command =
      CreateUserCommand.call(params[:user][:email], params[:user][:password])

    if user_command.success?
      flash.now[:notice] = 'Thanks, your email registered. Please confirm it.'
    else
      @errors = user_command.errors
    end

    render :new
  end

  def create_session
    token_command =
      AuthenticateUserCommand.call(
        params[:user][:email],
        params[:user][:password]
      )

    if token_command.success?
      cookies.signed[:user_id] = {
        value: token_command.result, expires: 30.days
      }
      redirect_to root_path
    else
      @errors = token_command.errors
      render :login
    end
  end

  def login; end

  def new; end

  def profile; end

  def favorited; end

  def logout
    cookies.signed[:user_id] = nil
    render json: { success: true }
  end

  def confirm
    confirm_command = ConfirmUserCommand.call(params[:token])

    flash.now[:notice] = if confirm_command.success?
                           I18n.t('confirm_user_command.success')
                         else
                           confirm_command.errors.full_messages.join('')
                         end

    render :confirm
  end

  private

  def users_params
    params.require(:user).permit(:email, :password)
  end
end
