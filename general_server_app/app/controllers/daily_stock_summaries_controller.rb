# frozen_string_literal: true

class DailyStockSummariesController < ApplicationController
  before_action :find_stock_symbol, only: %i[index]
  skip_before_action :authorize_user

  def index
    summaries = @stock_symbol.daily_stock_summaries
    render json: { data: summaries }
  end

  private

  def find_stock_symbol
    @stock_symbol = StockSymbol.find(params[:stock_symbol_id])
  end
end
