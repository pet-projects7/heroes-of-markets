# frozen_string_literal: true

class MarketsController < ApplicationController
  before_action :find_market, only: %i[show]
  skip_before_action :authorize_user

  def index
    @markets = Market.active.order(stock_symbols_count: :desc)

    respond_to do |format|
      format.html {}
      format.json { render json: { data: @markets } }
    end
  end

  def show; end

  def find_market
    @market = Market.friendly.find(params[:id]).decorate
  end
end
