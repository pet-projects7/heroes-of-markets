# frozen_string_literal: true

class ApplicationController < ActionController::Base
  include TokenAuthenticatable

  def not_found
    raise ActionController::RoutingError, 'Not Found'
  end
end
