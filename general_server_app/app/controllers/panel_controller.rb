# frozen_string_literal: true

class PanelController < ApplicationController
  private

  def authorize_user
    raise NotAuthorizedException unless current_user
    raise NotAuthorizedException unless current_user.has_role? :panel_user
  end
end
