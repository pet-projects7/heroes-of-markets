# frozen_string_literal: true

class ConfirmUserCommand < BaseCommand
  def initialize(token)
    @token = token
  end

  private

  def payload
    return errors.add(:base, error_text) if @token.empty?

    decoded = JwtService.decode(@token)

    return errors.add(:base, I18n.t('confirm_user_command.wrong_token')) if @token.nil?

    user = User.find_by(email: decoded['user'])

    if user
      user.update(confirmed: true)
      @result = user
    else
      errors.add(:base, I18n.t('confirm_user_command.wrong_token'))
    end
  end
end
