# frozen_string_literal: true

class StockSymbolDecorator < Draper::Decorator
  delegate_all

  def full_name
    if object.full_name.present? && object.full_name != 'N/A'
      "#{object.full_name} (#{object.title})"
    else
      object.title
    end
  end

  # Define presentation-specific methods here. Helpers are accessed through
  # `helpers` (aka `h`). You can override attributes, for example:
  #
  #   def created_at
  #     helpers.content_tag :span, class: 'time' do
  #       object.created_at.strftime("%a %m/%d/%y")
  #     end
  #   end
end
