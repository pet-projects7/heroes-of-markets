# frozen_string_literal: true

class MarketDecorator < Draper::Decorator
  delegate_all

  def title
    if object.title.present? && object.title != 'N/A'
      object.title
    else
      object.symbol
    end
  end
end
