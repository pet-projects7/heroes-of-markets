# frozen_string_literal: true

class PanelPolicy < ApplicationPolicy
  def index; end
end
