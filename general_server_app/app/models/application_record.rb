# frozen_string_literal: true

# Базовая модель для наследования
class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true
end
