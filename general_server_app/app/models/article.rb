# frozen_string_literal: true

class Article < ApplicationRecord
  has_rich_text :content
  validates :title, :content, :source_description, :source_link, presence: true

  has_many :article_stock_symbol_effects
  has_many :stock_symbols, through: :article_stock_symbol_effects
  has_many :markets, through: :stock_symbols

  accepts_nested_attributes_for :article_stock_symbol_effects
end
