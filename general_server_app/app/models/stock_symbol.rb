# frozen_string_literal: true

class StockSymbol < ApplicationRecord
  extend FriendlyId
  has_many :daily_stock_summaries, dependent: :delete_all
  has_many :user_favorite_symbols, dependent: :destroy
  has_many :users, through: :user_favorite_symbols
  has_many :article_stock_symbol_effects
  has_many :articles, through: :article_stock_symbol_effects
  has_many :market_results
  belongs_to :market, counter_cache: true

  enum state: { active: 0, archived: 1 }
  friendly_id :slug_candidates, use: :slugged

  has_many :children, class_name: 'StockSymbol', foreign_key: 'parent_stock_id'
  belongs_to :parent_stock, class_name: 'StockSymbol', optional: true

  def slug_candidates
    [:title, :full_name, %i[title full_name]]
  end

  delegate :slug, to: :market, prefix: true

  scope :with_lifetime_summary, -> {
    # Рынки где есть акции без саммари
    # StockSymbol.where(lifetime_summary: nil).select(:market_id).distinct.pluck :market_id
    where.not(lifetime_summary: nil)
  }
end
