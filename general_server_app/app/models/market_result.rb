class MarketResult < ApplicationRecord
  enum category: { leaders: 0, loosers: 1 }

  belongs_to :market
  belongs_to :stock_symbol

  validates :year,
            :month,
            :percent_result,
            :absolute_result,
            :category,
            presence: true
end
