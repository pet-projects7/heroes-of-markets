# frozen_string_literal: true

class UserFavoriteSymbol < ApplicationRecord
  belongs_to :user
  belongs_to :stock_symbol

  validates :user_id,
            uniqueness: {
              scope: :stock_symbol_id, message: 'this stock already favorited'
            }
end
