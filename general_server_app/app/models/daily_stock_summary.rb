# frozen_string_literal: true

# Модель для ежедневной статистики, не используется
class DailyStockSummary < ApplicationRecord
  belongs_to :stock_symbol
end
