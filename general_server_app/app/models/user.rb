# frozen_string_literal: true

# Стандартный пользователь, но авторизация не device
class User < ApplicationRecord
  rolify
  has_secure_password
  validates :email,
            format: { with: URI::MailTo::EMAIL_REGEXP },
            presence: true,
            uniqueness: { case_sensitive: false }

  # validates :password, presence: true

  after_create :send_confirmation_link, :assign_default_role

  has_many :user_favorite_symbols
  has_many :stock_symbols, through: :user_favorite_symbols

  private

  def send_confirmation_link
    UserEmailConfirmationMailer.confirm(self).deliver
  end

  def assign_default_role
    add_role(:basic) if roles.blank?
  end
end
