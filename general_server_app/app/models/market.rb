# frozen_string_literal: true

# Модель одного рынка акций, например Москвоская биржа
class Market < ApplicationRecord
  extend FriendlyId
  has_many :stock_symbols, dependent: :delete_all
  has_many :article_stock_symbol_effects, through: :stock_symbols
  has_many :articles, through: :article_stock_symbol_effects
  has_many :market_results, dependent: :delete_all

  enum state: { active: 0, archived: 1 }
  enum working_status: { closed: 0, opened: 1 }

  friendly_id :slug_candidates, use: :slugged

  def slug_candidates
    [:symbol, :title, %i[symbol title]]
  end

  def first_year # Первый год работы биржы
    stock_symbols
        .where.not(lifetime_summary: nil)
        .order("lifetime_summary -> 'years' DESC")
        .first
        .lifetime_summary['years']
        .keys
        .first
        .to_i
  end
end
