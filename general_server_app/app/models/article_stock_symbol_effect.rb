# frozen_string_literal: true

class ArticleStockSymbolEffect < ApplicationRecord
  self.table_name = 'articles_stock_symbols'
  belongs_to :article
  belongs_to :stock_symbol

  enum state: { positive: 0, negative: 1, neutral: 2 }
end
