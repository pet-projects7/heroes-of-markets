# frozen_string_literal: true

require 'httparty'

# symbol
# api_token
# date_from
# date_to
# sort
# output
# formatted
#
# ^XINH
class HistoryWorldTradingData
  include Interactor
  BASE_URI = 'https://www.worldtradingdata.com/api'

  def call
    context.stock_symbol = StockSymbol.find_or_create_by(title: context.symbol)

    response = fetch
    context.response = collect(response)
  end

  def get_json
    response = fetch

    return [] if response['history'].blank?

    response['history'].keys.collect do |date|
      {
        date: Date.parse(date),
        open: response['history'][date]['open'],
        close: response['history'][date]['close'],
        high: response['history'][date]['high'],
        low: response['history'][date]['low'],
        volume: response['history'][date]['volume']
      }
    end
  end

  private

  def fetch
    query = {
      symbol: context.symbol,
      api_token: Settings.world_trading_data.token
    }

    query[:date_from] = context.date_from if context.date_from
    query[:date_to] = context.date_to if context.date_to

    # Возвращает ответ с ключами: name, history
    response = HTTParty.get("#{BASE_URI}/v1/history", query: query)
  end

  def collect(response)
    return [] if response['history'].blank?

    response['history'].keys.collect do |date|
      {
        date: Date.parse(date),
        open: response['history'][date]['open'],
        close: response['history'][date]['close'],
        high: response['history'][date]['high'],
        low: response['history'][date]['low'],
        volume: response['history'][date]['volume'],
        stock_symbol_id: context.stock_symbol.id
      }
    end
  end
end
