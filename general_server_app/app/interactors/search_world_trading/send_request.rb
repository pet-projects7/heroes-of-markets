# frozen_string_literal: true

require 'httparty'

# search_term
# api_token
# search_by
# stock_exchange
# currency
# limit
# page
# sort_by
# sort_order

class SearchWorldTrading::SendRequest
  include Interactor

  def call(query_params = {})
    context.query = {
      api_token: Settings.world_trading_data.token,
      sort_order: :desc,
      limit: 200
    }

    context.query = context.query.merge query_params

    data = request
    context.stocks = convert_response(data)
  end

  def request
    url = "#{Settings.world_trading_data.default_url}/v1/stock_search"
    response = HTTParty.get(url, query: context.query)
    response['data']
  end

  def convert_response(stocks)
    stocks.map do |symbol|
      market_params = {
        symbol: symbol['stock_exchange_short'],
        title: symbol['stock_exchange_long']
      }
      market = Market.find_or_create_by(market_params)

      {
        title: symbol['symbol'],
        market_id: market.id,
        currency: symbol['currency'] == 'N/A' ? nil : symbol['currency'],
        full_name: symbol['name'],
        state: :active
      }
    end
  end
end
