# frozen_string_literal: true

require 'httparty'

class SearchWorldTrading::GetPagesCount
  include Interactor

  def call
    context.query = {
      api_token: Settings.world_trading_data.token,
      sort_order: :desc,
      limit: 200
    }
    context.pages_count = fetch_pages_count
  end

  def fetch_pages_count
    context.query[:stock_exchange] = context.market if context.market.present?

    # response.keys = ["message", "total_returned", "total_results", "total_pages", "limit", "page", "data"]
    url = "#{Settings.world_trading_data.default_url}/v1/stock_search"
    response = HTTParty.get(url, query: context.query)
    pages_count = response['total_pages']

    pages_count.to_i
  end
end
