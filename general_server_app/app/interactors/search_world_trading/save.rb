# frozen_string_literal: true

require 'httparty'

class SearchWorldTrading::Save
  include Interactor

  def call(stocks)
    StockSymbol.import(stocks, on_duplicate_key_ignore: true)
  end
end
