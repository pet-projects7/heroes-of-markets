# frozen_string_literal: true

require 'httparty'

class FetchIntradayStockMarketData
  include Interactor

  def call(query = {})
    raise ArgumentError, 'No symbol provided' if query[:symbol].blank?

    context.query = {
      api_token: Settings.world_trading_data.token,
      symbol: query[:symbol],
      range: query[:range] || 1,
      interval: query[:interval] || 1
    }

    request_data
  end

  private def request_data
    response =
      HTTParty.get "#{Settings.world_trading_data.intraday_url}/v1/intraday",
                   query: context.query
    JSON.parse(response.body)
  end
end
