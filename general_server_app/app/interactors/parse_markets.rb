# frozen_string_literal: true

class ParseMarkets
  include Interactor
  BASE_URL = 'https://www.stockmarketclock.com'

  require 'nokogiri'
  require 'open-uri'

  def call
    context.markets = fetch_markets_summary
  end

  private

  def fetch_markets_summary
    doc = Nokogiri.HTML(open("#{BASE_URL}/exchanges"))

    doc.css('#exchangetable tbody tr').map do |table_row|
      {
        title: table_row.css('[data-title="Name"] a').attribute('title').value,
        symbol: table_row.css('[data-title="Symbol"] b').text
      }
    end
  end
end
