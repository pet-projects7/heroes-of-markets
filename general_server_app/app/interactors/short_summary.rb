# frozen_string_literal: true

require 'httparty'

class ShortSummary
  include Interactor
  BASE_URI = 'https://www.worldtradingdata.com/api'

  def call
    context.response = fetch
  end

  private

  def fetch
    query = {
      symbol: context.symbol.join(','),
      api_token: Settings.world_trading_data.token
    }

    response = HTTParty.get("#{BASE_URI}/v1/stock", query: query)

    JSON.parse(response.body)['data']
  end
end
