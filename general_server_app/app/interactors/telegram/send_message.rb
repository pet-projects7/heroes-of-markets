# frozen_string_literal: true

require 'httparty'

# Отправка сообщений в телегу
class Telegram::SendMessage
  include Interactor

  # Отправка сообщения пользователю (сейчас только m.rukomoynikov)
  def call(message = '')
    telegram_settings = Settings.telegram
    query = { chat_id: telegram_settings.rukomoynikov_user_id, text: message }

    url = "https://api.telegram.org/bot#{telegram_settings.token}/sendMessage"

    context.response = HTTParty.post(url, body: query)
  rescue Net::ReadTimeout => e
    p 'Error when connecting to Telegram servers'
  end
end
