# frozen_string_literal: true

class UserFavoriteStockSerializer
  include FastJsonapi::ObjectSerializer
  # belongs_to :stock_symbol
  cache_options enabled: true, cache_length: 12.hours

  link :stock_symbol

  link :market do |object|
    object.stock_symbol.market
  end
end
