# frozen_string_literal: true

class StockSerializer
  include FastJsonapi::ObjectSerializer
  cache_options enabled: true, cache_length: 12.hours

  belongs_to :market
end
