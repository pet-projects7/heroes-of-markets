# frozen_string_literal: true
# namespace :client_app do
#   desc "Move client app"
#
#   task :move do
#     on roles(:app) do
#       within release_path do
#         execute `cd ../client_app && yarn --modules-folder #{shared_path}/npm`
#       end
#     end
#   end
#
# end
#
# namespace :git do
#   after :create_release, 'client_app:move'
# end
