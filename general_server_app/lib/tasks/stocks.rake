# frozen_string_literal: true

namespace :stocks do
  desc 'Задачи для работы с сервисом https://www.worldtradingdata.com/'

  task fetch_new_symbols: :environment do |_task, _args|
    pages_count = SearchWorldTrading::GetPagesCount.call.pages_count

    Telegram::SendMessage.new.call 'Just started searching new stocks'

    new_stocks = []

    pages_count.times do |page_num|
      stocks = SearchWorldTrading::SendRequest.new.call(page: page_num)
      saved_stocks = SearchWorldTrading::Save.new.call(stocks).ids
      new_stocks += saved_stocks
    end

    Telegram::SendMessage.new.call "Just finished parse new stocks, and parsed #{new_stocks.count} stocks "
  end
end
