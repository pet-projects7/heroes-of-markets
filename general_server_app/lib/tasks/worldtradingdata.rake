# frozen_string_literal: true

namespace :worldtradingdata do
  desc 'Задачи для работы с сервисом https://www.worldtradingdata.com/'

  task history: :environment do
    # https://www.worldtradingdata.com/documentation#historical-market-data
    Market.find_by(symbol: 'NASDAQ').stock_symbols.each do |sym|
      HistoryWorldTradingData.call symbol: sym.title,
                                   date_from: '1900-01-01',
                                   date_to: '2019-01-01'
    end
  end

  task search: :environment do
    # https://www.worldtradingdata.com/documentation#searching
    SearchWorldTradingData.call
  end

  # Удаление существующих рынков и символов, с последущим полным забором
  task full_parse_symbols: :environment do
    DailyStockSummary.delete_all
    StockSymbol.delete_all
    Market.delete_all

    SearchWorldTradingData.call
  end

  # Парсинг всей истории, всех существующих символов
  task full_parse_symbols_history: :environment do
    StockSymbol.pluck(:title).each do |symbol_title|
      ParseSymbolHistoryWorker.perform_async symbol: symbol_title
    end

    # Market.find_by_symbol('MCX').stock_symbols.each do |sym|
    #   HistoryWorldTradingData.call symbol: sym.title
    # end
  end

  # Парсинг всей истории, всех существующих символов
  task daily_parse_symbols_history: :environment do
    StockSymbol.pluck(:title).each do |symbol_title|
      ParseSymbolHistoryWorker.perform_async symbol: symbol_title,
                                             date_from: 15.days.ago.strftime('%d-%m-%Y')
    end
  end
end
