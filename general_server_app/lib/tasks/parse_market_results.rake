# frozen_string_literal: true

task parse_market_results: :environment do
  Telegram::SendMessage.new.call 'Start refreshing market results'

  MarketResult.delete_all

  # Market.where(slug: 'mcx').each do |market|
  Market.all.each do |market|
    MarketResult::FetchMonthWorker.new.perform(market.id)
  end

  Telegram::SendMessage.new.call 'Finish refreshing market results'
end
