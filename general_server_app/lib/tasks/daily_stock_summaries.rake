# frozen_string_literal: true

namespace :daily_stock_summaries do
  task full_parse: :environment do
    CollectStocksDataWorker.perform_async
  end
end
