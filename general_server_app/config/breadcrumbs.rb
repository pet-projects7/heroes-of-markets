# frozen_string_literal: true

crumb :root do
  link 'Home', root_path
end

crumb :markets do
  link 'Markets', markets_path
end

crumb :market_results do |options|
  parent :markets
  parent :market_by_hash, market: options[:market]

  title = "Leaders and loosers"
  link title,
       market_results_path(market_id: options[:market].id)
end

crumb :market do |market|
  title = market.title.presence || market.symbol
  link title, market_path(market)
  parent :markets
end

crumb :market_by_hash do |market|
  title = market[:market]['title'].presence || market[:market]['symbol']
  link title, market_path(market[:market].slug)
  parent :markets
end

crumb :stock_symbol do |options|
  parent :markets
  parent :market_by_hash, market: options[:market]

  title = options[:stock_symbol].full_name.presence || options[:symbol]
  link title,
       market_stock_symbol_path(
         id: options[:stock_symbol].id, market_id: options[:market].id
       )
end
