# frozen_string_literal: true

Rails.application.routes.draw do
  root to: 'markets#index'

  resources :markets, only: %i[index show] do
    resources :stock_symbols,
              only: %i[index show], controller: 'markets/stock_symbols' do
      get :search, on: :collection
      resources :daily_stock_summaries, only: %i[index], shallow: true
    end
    resources :results, on: :member, only: [:index], controller: 'markets/results'
  end

  resources :tools, only: %i[index] do
    get :covariance, on: :collection
  end

  resources :stock_symbols, only: %i[index] do
    get :search, on: :collection
  end

  resource :users, only: %i[new create] do
    get :login, on: :collection
    get :profile, on: :collection
    get :favorited, on: :collection, as: :favorited_stocks
    get :confirm, on: :collection
    post :create_session, on: :collection
    delete :logout, on: :collection
  end

  namespace :api do
    namespace :v1 do
      resource :auth, only: %i[create]
      get 'user_favorite_symbols',
          to: 'user_favorite_symbols#index', as: :user_favorited_stocks
      resources :stock_symbols,
                only: %i[short_summary], constraints: { id: /.+/ } do
        get 'user_favorite_symbols/check',
            to: 'user_favorite_symbols#check', as: :user_favorite_symbols_check
        post 'user_favorite_symbols/toggle',
             to: 'user_favorite_symbols#toggle',
             as: :user_favorite_symbols_toggle
        resources :daily_stock_summaries, only: %i[index], shallow: true
        get :short_summary, on: :member
      end

      get :intraday_stock_market_data,
          controller: 'intraday_stock_market_data', action: 'show'
    end
  end

  namespace :panel do
    root to: 'panel#index'
    resources :articles
  end

  mount Peek::Railtie => '/peek' if Rails.env.development?

  require 'sidekiq/web'
  Sidekiq::Web.set :sessions, false
  mount Sidekiq::Web => '/sidekiq'
  Sidekiq::Web.use Rack::Auth::Basic do |username, password|
    ActiveSupport::SecurityUtils.secure_compare(
      ::Digest::SHA256.hexdigest(username),
      ::Digest::SHA256.hexdigest('adminadmin')
    ) &
      ActiveSupport::SecurityUtils.secure_compare(
        ::Digest::SHA256.hexdigest(password),
        ::Digest::SHA256.hexdigest('passwordpassword')
      )
  end


  # mount PgHero::Engine, at: 'pghero' if Rails.env.production?
end
