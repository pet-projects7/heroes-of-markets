# frozen_string_literal: true

ActionMailer::Base.add_delivery_method :ses,
                                       AWS::SES::Base,
                                       access_key_id: Settings.ses.ACCESS_KEY,
                                       secret_access_key:
                                         Settings.ses.SECRET_KEY
