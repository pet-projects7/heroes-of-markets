# frozen_string_literal: true

every 1.day, at: '8:00 am' do
  rake 'daily_stock_summaries:full_parse' # Загрузка кратких итогов работы
  rake 'stocks:fetch_new_symbols' # Загрузка новых акций
end
