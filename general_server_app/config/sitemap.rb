# frozen_string_literal: true

# Set the host name for URL creation
SitemapGenerator::Sitemap.default_host = 'https://heroes-of-markets.com/'
SitemapGenerator::Sitemap.sitemaps_path = 'sitemaps/'

SitemapGenerator::Sitemap.create do
  Market.find_each do |market|
    add market_path(market)

    market.stock_symbols.find_each do |stock_symbol|
      add market_stock_symbol_path(
        id: stock_symbol.slug || stock_symbol.id, market_id: market.slug
      )
    end
  end
end
