# Сервисы получения информации:

* https://iextrading.com/developer/docs/#book
* https://www.worldtradingdata.com
* https://rapidapi.com/collection/stock-market-apis?utm_source=google&utm_medium=cpc&utm_campaign=1672506610_66477331858&utm_term=_b&utm_content=1t1&gclid=CjwKCAjw5dnmBRACEiwAmMYGOSWsBd1o33vADxy9WdZLAp_CeQmGWgIF1Mmu5f6a8Z9bM6cKiEVdvhoCvHkQAvD_BwE

[![Coverage report](https://gitlab.com/gitlab-org/gitlab-ce/badges/master/coverage.svg?job=coverage)](http://gitlab-org.gitlab.io/gitlab-ce/coverage-ruby)

**Загрузка краткой статистики для одной акции**

```
FetchStockSummaryWorker.new.perform(stock.id)
```
