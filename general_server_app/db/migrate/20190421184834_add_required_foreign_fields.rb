# frozen_string_literal: true

class AddRequiredForeignFields < ActiveRecord::Migration[5.2]
  def change
    # Необходимые связи
    add_reference :stock_symbols, :market, foreign_key: true
    add_index :daily_stock_summaries, %i[date stock_symbol_id]

    # Удаление timestamps
    remove_column :daily_stock_summaries, :created_at
    remove_column :daily_stock_summaries, :updated_at

    remove_column :markets, :created_at
    remove_column :markets, :updated_at

    remove_column :stock_symbols, :created_at
    remove_column :stock_symbols, :updated_at

    add_column :markets, :symbol, :string
    add_column :markets, :site, :string, unique: true
  end
end
