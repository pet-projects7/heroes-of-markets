# frozen_string_literal: true

class AddCurrencyToSymbol < ActiveRecord::Migration[5.2]
  def change
    add_column :stock_symbols, :currency, :string
    add_column :stock_symbols, :full_name, :string
  end
end
