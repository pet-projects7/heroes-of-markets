class CreateMarketDirections < ActiveRecord::Migration[5.2]
  def change
    create_table :market_directions do |t|
      t.belongs_to :market, foreign_key: true
      t.integer :result
      t.date :date

      t.timestamps
    end
  end
end
