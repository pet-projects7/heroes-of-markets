# frozen_string_literal: true

class AddWorkingStatusToMarket < ActiveRecord::Migration[5.2]
  def change
    add_column :markets, :working_status, :integer
  end
end
