# frozen_string_literal: true

class CreateMarkets < ActiveRecord::Migration[5.2]
  def change
    create_table :markets do |t|
      t.string :title
      t.string :description

      t.timestamps
    end
  end
end
