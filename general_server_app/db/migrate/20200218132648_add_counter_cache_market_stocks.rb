class AddCounterCacheMarketStocks < ActiveRecord::Migration[5.2]
  def change
    add_column :markets, :stock_symbols_count, :integer

    Market.find_each { |market| Market.reset_counters(market.id, :stock_symbols) }
  end
end
