# frozen_string_literal: true

class AddSlugToMarkets < ActiveRecord::Migration[5.2]
  def change
    add_column :markets, :slug, :string
    add_index :markets, :slug, unique: true

    add_column :stock_symbols, :slug, :string
    add_index :stock_symbols, :slug, unique: true
  end
end
