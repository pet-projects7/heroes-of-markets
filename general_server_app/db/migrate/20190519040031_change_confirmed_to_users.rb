# frozen_string_literal: true

class ChangeConfirmedToUsers < ActiveRecord::Migration[5.2]
  def change
    change_column :users,
                  :confirmed,
                  :boolean,
                  default: false, using: 'confirmed::boolean'

    remove_column :users, :admin
  end
end
