# frozen_string_literal: true

class CreateJoinTableArticleStockSymbols < ActiveRecord::Migration[5.2]
  def change
    remove_column :articles, :stock_symbol_id if column_exists?(:articles, :stock_symbol_id)

    create_join_table :articles, :stock_symbols, id: true do |t|
      t.timestamps
      t.integer :effect
      t.index %i[article_id stock_symbol_id]
      t.index %i[stock_symbol_id article_id]
    end
  end
end
