# frozen_string_literal: true

class AddPrimaryKeyToArticlesStockSymbols < ActiveRecord::Migration[5.2]
  def change
    add_column :articles_stock_symbols, :id, :primary_key
  end
end
