# frozen_string_literal: true

class AddParentToStockSymbol < ActiveRecord::Migration[5.2]
  def change
    add_reference :stock_symbols,
                  :parent_stock,
                  foreign_key: { to_table: :stock_symbols }
  end
end
