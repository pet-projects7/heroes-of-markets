# frozen_string_literal: true

class CreateArticles < ActiveRecord::Migration[5.2]
  def change
    create_table :articles do |t|
      t.text :content
      t.string :title
      t.belongs_to :stock_symbol, foreign_key: true

      t.timestamps
    end
  end
end
