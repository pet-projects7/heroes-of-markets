# frozen_string_literal: true

class AddTimestampsToStocks < ActiveRecord::Migration[5.2]
  def change
    # add new column but allow null values
    add_timestamps :stock_symbols, null: true

    # backfill existing record with created_at and updated_at
    # values making clear that the records are faked
    long_ago = DateTime.new(2_000, 1, 1)
    StockSymbol.update_all(created_at: long_ago, updated_at: long_ago)

    # change not null constraints
    change_column_null :stock_symbols, :created_at, false
    change_column_null :stock_symbols, :updated_at, false
  end
end
