# frozen_string_literal: true

class CreateStocks < ActiveRecord::Migration[5.2]
  def change
    create_table :stock_symbols do |t|
      t.string :title

      t.timestamps
    end

    create_table :daily_stock_summaries do |t|
      t.datetime :date
      t.float :open
      t.float :close
      t.float :high
      t.float :low
      t.float :volume
      t.belongs_to :stock_symbol, foreign_key: true

      t.timestamps
    end

    add_index :stock_symbols, :title, unique: true
  end
end
