# frozen_string_literal: true

class CreateUserFavoriteSymbols < ActiveRecord::Migration[5.2]
  def change
    create_table :user_favorite_symbols do |t|
      t.belongs_to :user, foreign_key: true
      t.belongs_to :stock_symbol, foreign_key: true

      t.timestamps
    end
  end
end
