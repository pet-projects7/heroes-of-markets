class CreateMarketResults < ActiveRecord::Migration[5.2]
  def change
    create_table :market_results do |t|
      t.references :market
      t.references :stock_symbol
      t.integer :year
      t.integer :month
      t.float :percent_result
      t.float :absolute_result
      t.integer :category

      t.timestamps
    end
  end
end
