# frozen_string_literal: true

class AddStockSymbolsSummaryToStocksSymbol < ActiveRecord::Migration[5.2]
  def change
    add_column :stock_symbols, :lifetime_summary, :jsonb
  end
end
