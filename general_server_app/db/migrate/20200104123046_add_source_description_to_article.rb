# frozen_string_literal: true

class AddSourceDescriptionToArticle < ActiveRecord::Migration[5.2]
  def change
    add_column :articles, :source_description, :string
    add_column :articles, :source_link, :string
  end
end
