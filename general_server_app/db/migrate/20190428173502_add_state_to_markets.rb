# frozen_string_literal: true

class AddStateToMarkets < ActiveRecord::Migration[5.2]
  def change
    add_column :markets, :state, :integer
    add_column :stock_symbols, :state, :integer
  end
end
