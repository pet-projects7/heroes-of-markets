class AddCommentsToDatabase < ActiveRecord::Migration[5.2]
  def change
    change_table_comment :articles, 'Статьи'
    change_column_comment :articles, :content, 'Основной текст статьи'
    change_column_comment :articles, :title, 'Заголовок статьи'
    change_column_comment :articles, :source_description, 'Описание источника статьи'
    change_column_comment :articles, :source_link, 'Ссылка на источник статьи'

    change_table_comment :articles_stock_symbols, 'Таблица для связки статья - акция, многие ко многим'
    change_column_comment :articles_stock_symbols, :effect, 'Какой эффект производит новость на акцию'

    change_table_comment :daily_stock_summaries, 'Страница для хранения дневных результатов торгов акции (Не используется)'

    change_table_comment :markets, 'Рынки на которых проводятся торги'
    change_column_comment :markets, :title, 'Название'
    change_column_comment :markets, :description, 'Описание'
    change_column_comment :markets, :symbol, 'Тикер'
    change_column_comment :markets, :site, 'Основной сайт'
    change_column_comment :markets, :slug, 'Сгенерированный слаг'
    change_column_comment :markets, :state, 'Метка активна ли запись, некоторые рынки не используются'
    change_column_comment :markets, :working_status, 'Статус работает ли рынок сейчас, пока механизм не работает'

    change_table_comment :roles, 'Служебная таблица гема rolify'
    change_column_comment :roles, :name, 'Наименование роли'

    change_table_comment :stock_symbols, 'Акции'
    change_column_comment :stock_symbols, :title, 'Тикер (уникальное имя)'
    change_column_comment :stock_symbols, :market_id, 'Ссылка на рынок на котором торгуется акция'
    change_column_comment :stock_symbols, :currency, 'Валюта'
    change_column_comment :stock_symbols, :full_name, 'Наименование акции'
    change_column_comment :stock_symbols, :slug, 'Автоматически генерируемое имя'
    change_column_comment :stock_symbols, :state, 'Состояние, активно/не активно'
    change_column_comment :stock_symbols,
                          :lifetime_summary,
                          'Краткий отчёт по результатам работы. Содержит годы, и месяцы в каждого рамках года'
    change_column_comment :stock_symbols,
                          :parent_stock_id,
                          '
                            Ссылка на основной тикер, возможно на другой бирже.
                             Например для AAPL.MI тут будет ссылка на AAPL
                          '

    change_table_comment :user_favorite_symbols, 'Отслеживаемые пользователем акции'
  end
end
