# frozen_string_literal: true

class AddConfirmedToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :confirmed, :integer
  end
end
