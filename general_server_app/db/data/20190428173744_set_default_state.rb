# frozen_string_literal: true

class SetDefaultState < ActiveRecord::Migration[5.2]
  def up
    Market.update_all(state: 0)
    StockSymbol.update_all(state: 0)
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
