# frozen_string_literal: true

class FillMarkets < ActiveRecord::Migration[5.2]
  def up
    result = ParseMarkets.call
    Market.import result.markets
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
