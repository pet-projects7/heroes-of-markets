# frozen_string_literal: true

class SetMarketsByType < ActiveRecord::Migration[5.2]
  def up
    Market.update_all(state: :archived)

    Market.where(
      slug: %w[
        tsx
        lse
        fra
        ger
        nasdaq
        nyse
        nse
        asx
        six
        otcmkts
        mcx
        mil
        tase
        tsxv
        bse
        nysearca
        mad
        byst
        amex
        mex
        omx
        wse
        par
        nzx
        omxc
        jsx
        vse
        bcba
        sao
        omxh
        aex
        bats
        cnq
        doh
        jse
        omxt
        set
        ses
        tadawul
        ksc
        kls
        twse
        hkex
        bom
        szce
      ]
    )
      .update(state: :active)
  end

  def down; end
end
