# frozen_string_literal: true

class CreateSlugs < ActiveRecord::Migration[5.2]
  def up
    Market.find_each(&:save)
    StockSymbol.find_each(&:save)
  end

  def down; end
end
