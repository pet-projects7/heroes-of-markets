# frozen_string_literal: true

class ToggleDefaultWorkingStateToMarkets < ActiveRecord::Migration[5.2]
  def up
    Market.update_all(working_status: :closed)
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
