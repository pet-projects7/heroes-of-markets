# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_02_28_171836) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "action_text_rich_texts", force: :cascade do |t|
    t.string "name", null: false
    t.text "body"
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["record_type", "record_id", "name"], name: "index_action_text_rich_texts_uniqueness", unique: true
  end

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "articles", comment: "Статьи", force: :cascade do |t|
    t.text "content", comment: "Основной текст статьи"
    t.string "title", comment: "Заголовок статьи"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "source_description", comment: "Описание источника статьи"
    t.string "source_link", comment: "Ссылка на источник статьи"
  end

  create_table "articles_stock_symbols", comment: "Таблица для связки статья - акция, многие ко многим", force: :cascade do |t|
    t.bigint "article_id", null: false
    t.bigint "stock_symbol_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "effect", comment: "Какой эффект производит новость на акцию"
    t.index ["article_id", "stock_symbol_id"], name: "index_articles_stock_symbols_on_article_id_and_stock_symbol_id"
    t.index ["stock_symbol_id", "article_id"], name: "index_articles_stock_symbols_on_stock_symbol_id_and_article_id"
  end

  create_table "daily_stock_summaries", comment: "Страница для хранения дневных результатов торгов акции (Не используется)", force: :cascade do |t|
    t.datetime "date"
    t.float "open"
    t.float "close"
    t.float "high"
    t.float "low"
    t.float "volume"
    t.bigint "stock_symbol_id"
    t.index ["date", "stock_symbol_id"], name: "index_daily_stock_summaries_on_date_and_stock_symbol_id"
    t.index ["stock_symbol_id"], name: "index_daily_stock_summaries_on_stock_symbol_id"
  end

  create_table "data_migrations", primary_key: "version", id: :string, force: :cascade do |t|
  end

  create_table "friendly_id_slugs", force: :cascade do |t|
    t.string "slug", null: false
    t.integer "sluggable_id", null: false
    t.string "sluggable_type", limit: 50
    t.string "scope"
    t.datetime "created_at"
    t.index ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true
    t.index ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type"
    t.index ["sluggable_type", "sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_type_and_sluggable_id"
  end

  create_table "market_directions", force: :cascade do |t|
    t.bigint "market_id"
    t.integer "result"
    t.date "date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["market_id"], name: "index_market_directions_on_market_id"
  end

  create_table "market_results", force: :cascade do |t|
    t.bigint "market_id"
    t.bigint "stock_symbol_id"
    t.integer "year"
    t.integer "month"
    t.float "percent_result"
    t.float "absolute_result"
    t.integer "category"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["market_id"], name: "index_market_results_on_market_id"
    t.index ["stock_symbol_id"], name: "index_market_results_on_stock_symbol_id"
  end

  create_table "markets", comment: "Рынки на которых проводятся торги", force: :cascade do |t|
    t.string "title", comment: "Название"
    t.string "description", comment: "Описание"
    t.string "symbol", comment: "Тикер"
    t.string "site", comment: "Основной сайт"
    t.string "slug", comment: "Сгенерированный слаг"
    t.integer "state", comment: "Метка активна ли запись, некоторые рынки не используются"
    t.integer "working_status", comment: "Статус работает ли рынок сейчас, пока механизм не работает"
    t.integer "stock_symbols_count"
    t.index ["slug"], name: "index_markets_on_slug", unique: true
  end

  create_table "roles", comment: "Служебная таблица гема rolify", force: :cascade do |t|
    t.string "name", comment: "Наименование роли"
    t.string "resource_type"
    t.bigint "resource_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["name", "resource_type", "resource_id"], name: "index_roles_on_name_and_resource_type_and_resource_id"
    t.index ["resource_type", "resource_id"], name: "index_roles_on_resource_type_and_resource_id"
  end

  create_table "stock_symbols", comment: "Акции", force: :cascade do |t|
    t.string "title", comment: "Тикер (уникальное имя)"
    t.bigint "market_id", comment: "Ссылка на рынок на котором торгуется акция"
    t.string "currency", comment: "Валюта"
    t.string "full_name", comment: "Наименование акции"
    t.string "slug", comment: "Автоматически генерируемое имя"
    t.integer "state", comment: "Состояние, активно/не активно"
    t.jsonb "lifetime_summary", comment: "Краткий отчёт по результатам работы. Содержит годы, и месяцы в каждого рамках года"
    t.bigint "parent_stock_id", comment: "\n                            Ссылка на основной тикер, возможно на другой бирже.\n                             Например для AAPL.MI тут будет ссылка на AAPL\n                          "
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["market_id"], name: "index_stock_symbols_on_market_id"
    t.index ["parent_stock_id"], name: "index_stock_symbols_on_parent_stock_id"
    t.index ["slug"], name: "index_stock_symbols_on_slug", unique: true
    t.index ["title"], name: "index_stock_symbols_on_title", unique: true
  end

  create_table "user_favorite_symbols", comment: "Отслеживаемые пользователем акции", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "stock_symbol_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["stock_symbol_id"], name: "index_user_favorite_symbols_on_stock_symbol_id"
    t.index ["user_id"], name: "index_user_favorite_symbols_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", null: false
    t.string "password_digest", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "confirmed", default: false
  end

  create_table "users_roles", id: false, force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "role_id"
    t.index ["role_id"], name: "index_users_roles_on_role_id"
    t.index ["user_id", "role_id"], name: "index_users_roles_on_user_id_and_role_id"
    t.index ["user_id"], name: "index_users_roles_on_user_id"
  end

  add_foreign_key "active_storage_attachments", "active_storage_blobs", column: "blob_id"
  add_foreign_key "daily_stock_summaries", "stock_symbols"
  add_foreign_key "market_directions", "markets"
  add_foreign_key "stock_symbols", "markets"
  add_foreign_key "stock_symbols", "stock_symbols", column: "parent_stock_id"
  add_foreign_key "user_favorite_symbols", "stock_symbols"
  add_foreign_key "user_favorite_symbols", "users"
end
