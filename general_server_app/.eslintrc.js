module.exports = {
  "plugins": ["prettier"],
  "parserOptions": {
    "parser": "@typescript-eslint/parser"
  },
  extends: [
    "plugin:vue/recommended"
  ],
  "rules": {
    "prettier/prettier": "error"
  }
}
