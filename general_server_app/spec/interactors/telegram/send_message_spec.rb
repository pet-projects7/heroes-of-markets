# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Telegram::SendMessage, type: :interactor do
  describe '.call', :vcr do
    it 'sends a message' do
      expect(described_class.call.response.parsed_response.keys).to eq %w[
        ok
        result
      ]
    end
  end
end
