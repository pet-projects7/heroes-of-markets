# frozen_string_literal: true

require 'rails_helper'

RSpec.describe SearchWorldTrading::SendRequest, :vcr, type: :interactor do
  describe '.call' do
    it 'returns stocks' do
      expect(described_class.call.stocks).not_to be nil
    end

    it 'creates missing markets' do
      expect(Market.count).to eq 0
      described_class.call
      expect(Market.count).to eq 10
    end
  end
end
