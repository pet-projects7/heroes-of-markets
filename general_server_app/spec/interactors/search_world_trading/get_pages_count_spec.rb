# frozen_string_literal: true

require 'rails_helper'

RSpec.describe SearchWorldTrading::GetPagesCount, :vcr, type: :interactor do
  describe '.call' do
    it 'gives pages count' do
      expect(SearchWorldTrading::GetPagesCount.call.pages_count).to eq 331
    end
  end
end
