# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ParseMarkets, type: :interactor do
  describe '.call' do
    it '' do
      VCR.use_cassette('ParseMarkets', record: :new_episodes) do
        result = described_class.call

        expect(result.markets.count).to equal(96)
      end
    end
  end
end
