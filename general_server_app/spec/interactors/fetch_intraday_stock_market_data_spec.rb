# frozen_string_literal: true

require 'rails_helper'

RSpec.describe FetchIntradayStockMarketData, type: :interactor do
  describe '.call', :vcr do
    it 'returns fail without symbol in params' do
      expect { described_class.new.call }.to raise_error ArgumentError
    end

    it 'returns the correct json' do
      request = described_class.new.call(symbol: 'AAPL')

      expect(request.keys).to eq %w[
        symbol
        stock_exchange_short
        timezone_name
        intraday
      ]
    end
  end
end
