# frozen_string_literal: true

require 'rails_helper'

RSpec.describe HistoryWorldTradingData, type: :interactor do
  describe '.call' do
    it '' do
      VCR.use_cassette('HistoryWorldTradingData', record: :new_episodes) do
        result =
          described_class.call symbol: 'SBER.ME',
                               date_from: '1900-01-01',
                               date_to: '2019-01-01'

        expect(result).to be_truthy
      end
    end
  end
end
