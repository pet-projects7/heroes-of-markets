# frozen_string_literal: true

module FeatureHelpers
  def sign_in(user)
    visit login_users_path
    fill_in 'Email', with: user.email
    fill_in 'Password', with: user.password
    click_on 'Send'
  end
end

RSpec.configure do |config|
  config.include FeatureHelpers, type: :feature
end
