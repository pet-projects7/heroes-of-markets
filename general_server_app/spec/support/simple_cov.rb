# frozen_string_literal: true

require 'simplecov'
SimpleCov.start 'rails' do
  add_filter '/spec/'
  add_filter '/bin/'
  add_filter '/config/'
  add_filter '/vendor'
end
