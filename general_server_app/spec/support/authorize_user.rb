# frozen_string_literal: true

RSpec.configure do |config|
  config.before(:each, authorized: true) { authenticate_user }

  config.after(:each, authorized: true) { Capybara.reset_sessions! }
end
