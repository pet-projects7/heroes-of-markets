# frozen_string_literal: true

VCR.configure do |c|
  c.cassette_library_dir = 'spec/vcr'
  c.hook_into :webmock
  c.ignore_localhost = true
  # c.debug_logger = $stderr
  c.ignore_hosts 'chromedriver.storage.googleapis.com'

  c.allow_http_connections_when_no_cassette = true
  c.configure_rspec_metadata!
  c.default_cassette_options = { match_requests_on: %i[uri], record: :once }
end
