# frozen_string_literal: true

require 'rails_helper'

RSpec.describe StockSymbolsQuery, type: :interactor do
  describe '.search' do
    let!(:market) { create(:market) }
    let!(:stock_symbols) { create_list(:stock_symbol, 7, market: market) }

    context 'when giving only title' do
      it 'returns all variants without query' do
        results = described_class.new.search({})

        expect(results.count).to equal(7)
      end

      it 'returns more than 1 variant' do
        results = described_class.new.search(search_query: stock_symbols.first.full_name)

        expect(results.count).to be_between(1,7)
      end

      it 'returns zero variants' do
        results =
          described_class.new(Market.first.stock_symbols).search(
            search_query: 'APAPAP'
          )
        expect(results.count).to equal(0)
      end
    end
  end
end
