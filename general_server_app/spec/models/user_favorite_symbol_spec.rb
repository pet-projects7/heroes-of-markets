# frozen_string_literal: true

require 'rails_helper'

RSpec.describe UserFavoriteSymbol, type: :model do
  let(:stock_symbol) { create(:stock_symbol) }
  let(:user2) { create(:user) }
  let(:user) { create(:user) }

  it { is_expected.to belong_to(:user) }
  it { is_expected.to belong_to(:stock_symbol) }

  describe 'symbol and user should be unique' do
    it 'gives an error when same user add to favorite same symbol' do
      user.user_favorite_symbols.create stock_symbol: stock_symbol
      expect do
        user.user_favorite_symbols.create! stock_symbol: stock_symbol
      end.to raise_error ActiveRecord::RecordInvalid
    end

    it 'is ok when different users use same symbol' do
      user.user_favorite_symbols.create stock_symbol: stock_symbol
      user2.user_favorite_symbols.create stock_symbol: stock_symbol
    end
  end
end
