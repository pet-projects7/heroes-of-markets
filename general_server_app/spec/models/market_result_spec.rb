require 'rails_helper'

RSpec.describe MarketResult, type: :model do
  it { is_expected.to belong_to(:market) }
  it { is_expected.to belong_to(:stock_symbol) }
end
