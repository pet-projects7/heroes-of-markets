# frozen_string_literal: true

require 'rails_helper'

RSpec.describe User, type: :model do
  it { is_expected.to have_many(:user_favorite_symbols) }

  describe '.save' do
    it 'creates confirmation email after user creation' do
      user = build(:user)
      user.save

      expect(ActionMailer::Base.deliveries.empty?).to be(false)
    end

    it 'will show error with user without password' do
      user = build(:user)
      user.email = ''

      expect { user.save! }.to raise_error ActiveRecord::RecordInvalid
    end
  end
end
