# frozen_string_literal: true

RSpec.describe Market, type: :model do
  it { is_expected.to have_many(:stock_symbols) }
  it { is_expected.to have_many(:articles) }
  it { is_expected.to have_many(:market_results) }
end
