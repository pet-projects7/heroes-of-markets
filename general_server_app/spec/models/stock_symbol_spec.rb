# frozen_string_literal: true

require 'rails_helper'

RSpec.describe StockSymbol, type: :model do
  it { is_expected.to belong_to(:market) }
  it { is_expected.to have_many(:articles) }
  it { is_expected.to have_many(:market_results) }

  describe '.market_slug' do
    let(:stock_symbol) { create(:stock_symbol, title: 'Hey hey') }

    it 'has a market slug' do
      expect(stock_symbol.market_slug).to eq('kls')
    end
  end
end
