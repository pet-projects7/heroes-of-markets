# frozen_string_literal: true

RSpec.describe DailyStockSummary, type: :model do
  it { is_expected.to belong_to(:stock_symbol) }
end
