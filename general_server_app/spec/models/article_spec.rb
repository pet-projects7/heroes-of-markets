# frozen_string_literal: true

RSpec.describe Article, type: :model do
  it { is_expected.to have_many(:stock_symbols) }
  it { is_expected.to have_many(:markets) }

  it { is_expected.to validate_presence_of(:title) }
  it { is_expected.to validate_presence_of(:content) }
  it { is_expected.to validate_presence_of(:source_description) }
  it { is_expected.to validate_presence_of(:source_link) }
end
