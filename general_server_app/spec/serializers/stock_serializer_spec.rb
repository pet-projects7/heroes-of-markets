# frozen_string_literal: true

# require 'rails_helper'
#
# RSpec.describe StockSerializer do
#   let(:stock) { create(:stock_symbol) }
#   let(:serialized_data) {
#     { data: {
#         id: "152",
#         type: :stock,
#         relationships: {
#           market: {
#             data: {
#               id: "87",
#               type: :market
#             }
#           }
#         }
#       }
#     }
#   }
#
#   describe '.call' do
#     it 'gives pages count' do
#       expect(StockSerializer.new(stock).to_hash).to eq serialized_data
#     end
#   end
# end
