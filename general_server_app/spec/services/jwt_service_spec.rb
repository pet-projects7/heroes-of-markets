# frozen_string_literal: true

require 'rails_helper'

RSpec.describe JwtService, type: :service do
  let(:token) do
    'eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxLCJleHAiOjYzMzc0NDAwMH0.Ort9kbKTtv3WFV9VYhmfTIBnr1PS1otoR_s-zYWjKjo'
  end

  before { Timecop.freeze(Time.zone.local(1_990)) }

  after { Timecop.return }

  context 'with valid tokens' do
    describe '.encode' do
      it 'creates token' do
        encoded =
          described_class.encode(
            user_id: 1, exp: 30.days.since(Time.zone.today).strftime('%s').to_i
          )
        expect(encoded).to eq(token)
      end
    end

    describe '.decode' do
      it 'decode token' do
        decoded = described_class.decode(token)
        expect(decoded['user_id']).to eq(1)
      end
    end
  end

  context 'with invalid tokens' do
    describe 'expired token' do
      it 'gets nil' do
        encoded = described_class.encode(user_id: 1, exp: 1.day.ago.to_i)
        expect(described_class.decode(encoded)).to be_nil
      end
    end

    describe 'expired token' do
      it 'gets nil' do
        encoded = described_class.encode(user_id: 1, exp: 1.day.from_now.to_i)
        expect(described_class.decode("#{encoded}ksjf")).to be_nil
      end
    end
  end
end
