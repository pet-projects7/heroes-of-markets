# frozen_string_literal: true

FactoryBot.define do
  factory :market do
    sequence (:title) { |n| titles[n] || 'Kuala Lumpur' }
    sequence (:symbol) { |n| slugs[n] || 'kls' }

    trait :active do
      state { :active }
    end
  end
end

def titles
  [ "NYSE American Stock Exchange", "Kuala Lumpur", "Bombay Stock Exchange", "Istanbul Stock Exchange", "Hong Kong Stock Exchange", "SES", "Thailand", "Tallinn Stock Exchange", "Saudi Stock Exchange", "Korean Stock Exchange", "Taiwan Stock Exchange", "Shenzhen Stock Exchange", "Vienna Stock Exchange", "New Zealand Stock Exchange", "Euronext Paris", "Warsaw Stock Exchange", "Stockholm Stock Exchange", "Johannesburg Stock Exchange", "Canadian Securities Exchange", "Euronext Amsterdam", "Helsinki Stock Exchange", "Qatar", "Madrid Stock Exchange", "TSX Venture Exchange", "Tel Aviv Stock Exchange", "Milan Stock Exchange", "Moscow Stock Exchange", "London Stock Exchange", "Deutsche Börse XETRA", "SIX Swiss Exchange", "National Stock Exchange of India", "Australian Securities Exchange", "OTC Markets Group", "Frankfurt Stock Exchange", "Toronto Stock Exchange", "Brussels Stock Exchange", "Mexican Stock Exchange", "New York Stock Exchange", "NASDAQ Stock Exchange", "Copenhagen Stock Exchange", "Jakarta Stock Exchange", "NYSE Arca (Archipelago Exchange)", "Buenos Aires Stock Exchange", "Sao Paolo Stock Exchange", "BATS Global Markets" ]
end

def slugs
  [ "nyse", "kls", "bom", "bist", "hkex", "ses", "set", "omxt", "tadawul", "ksc", "twse", "szce", "vse", "nzx", "par", "wse", "omx", "jse", "cnq", "aex", "omxh", "doh", "amex", "mad", "tsxv", "tase", "mil", "mcx", "lse", "ger", "six", "nse", "asx", "otcmkts", "fra", "tsx", "bse", "mex", "nasdaq", "omxc", "jsx", "nysearca", "bcba", "sao", "bats"]
end
