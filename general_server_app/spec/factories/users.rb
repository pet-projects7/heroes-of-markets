# frozen_string_literal: true

FactoryBot.define do
  factory :user do
    email { Faker::Internet.email }
    password { Faker::Company.bs }

    trait :confirmed do
      confirmed { true }
    end

    factory :panel_user do
      confirmed { true }
      after(:create) do |user|
        user.add_role :panel_user
      end
    end
  end
end
