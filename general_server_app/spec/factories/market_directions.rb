FactoryBot.define do
  factory :market_direction do
    market { nil }
    result { 1 }
    date { "2020-02-28" }
  end
end
