# frozen_string_literal: true

FactoryBot.define do
  factory :article do
    content { Faker::Lorem.paragraph }
    title { Faker::Game.title }
    stock_symbols { [create(:stock_symbol, title: Faker::Company.bs)] }
    source_description { Faker::Game.title }
    source_link { Faker::Game.title }
  end
end
