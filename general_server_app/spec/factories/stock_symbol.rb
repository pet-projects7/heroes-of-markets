# frozen_string_literal: true

FactoryBot.define do
  factory :stock_symbol do
    currency { 'USD' }
    sequence(:title) { |index| titles(index) }
    sequence(:full_name) { |index| full_names(index) }
    lifetime_summary { lifetime_summary_json }
    market
  end
end

def titles(index = 0)
  ["AAPL", "AGRO.ME", "CLSB.ME", "DZRDP.ME", "MOBB.ME", "NAUK.ME", "NSVZ.ME", "RBCM.ME", "SELG.ME", "YNDX.ME", "ARSA.ME", "RTGZ.ME", "RTSBP.ME", "RUSI.ME", "UNAC.ME", "UNKL.ME", "ABRD.ME", "BANE.ME", "KAZT.ME", "KCHEP.ME", "MAGE.ME", "MSRS.ME", "PLSM.ME", "RLMN.ME", "SBERP.ME", "TASB.ME", "AQUA.ME", "BISVP.ME", "CLSBP.ME", "LVHK.ME", "MFGS.ME", "MGNZ.ME", "NFAZ.ME", "ALNU.ME", "MISB.ME", "MSST.ME", "NVTK.ME", "PHOR.ME", "RSTI.ME", "RUSP.ME", "TATN.ME", "GAZA.ME", "GAZC.ME", "HYDR.ME", "KRSB.ME", "MGTS.ME", "MRKP.ME", "MSNG.ME", "YASH.ME", "YKEN.ME", "ELTZ.ME", "IRAO.ME", "MRKS.ME", "LNTA.ME", "TRCN.ME", "AFKS.ME", "CNTLP.ME", "RZSB.ME", "VJGZ.ME", "YAKG.ME", "KTSB.ME", "USBN.ME", "CHGZ.ME", "HIMCP.ME", "QIWI.ME", "VGSB.ME", "NKSH.ME", "PAZA.ME", "SBER.ME", "GMKN.ME", "DASB.ME", "MTSS.ME", "NKNC.ME", "SVAV.ME", "PRMB.ME", "URKZ.ME", "NNSB.ME", "OGKB.ME", "FEES.ME", "TANL.ME", "TNSE.ME", "OMSH.ME", "OPIN.ME", "PLZL.ME", "UWGN.ME", "ZHIV.ME", "RNFT.ME", "WTCMP.ME", "MERF.ME", "TGKO.ME", "SIBG.ME", "HALS.ME", "CHKZ.ME", "TUZA.ME", "ZVEZ.ME", "AKRN.ME", "MSTT.ME", "TUCH.ME", "NMTP.ME", "UKUZ.ME", "LIFE.ME", "ROLO.ME", "LSNG.ME", "VLHZ.ME", "KOGK.ME", "KLSB.ME", "KUBE.ME", "CHMK.ME", "DIOD.ME", "SZPR.ME", "MTLR.ME", "ISKJ.ME", "PMSB.ME", "OTCP.ME", "RTSB.ME", "PMSBP.ME", "RKKE.ME", "YRSB.ME", "MVID.ME", "GAZT.ME", "TTLK.ME", "RTKM.ME", "RLMNP.ME", "RNAV.ME", "RTKMP.ME", "RTSSTD.ME", "RUALR.ME", "TORSP.ME", "DSKY.ME", "PIKK.ME", "HIMC.ME", "POLY.ME", "PRFN.ME", "JNOSP.ME", "UTAR.ME", "ALRS.ME", "KAZTP.ME", "STSBP.ME", "KGKCP.ME", "APTK.ME", "FTRE.ME", "VRSB.ME", "TATNP.ME", "UTSY.ME", "KRKO.ME", "KRKNP.ME", "KRKOP.ME", "KRSBP.ME", "KTSBP.ME", "AVAN.ME", "TRMK.ME", "SARE.ME", "KZOSP.ME", "WTCM.ME", "VRAO.ME", "VRAOP.ME", "ARMD.ME", "VTRS.ME", "STSB.ME", "KRKN.ME", "VZRZP.ME", "BRZL.ME", "MISBP.ME", "BANEP.ME", "MORI.ME", "ZMZNP.ME", "ENPL.ME", "LNZLP.ME", "IDVP.ME", "YKENP.ME", "YRSBP.ME", "ZMZN.ME", "EPLN.ME", "GAZS.ME", "MAGEP.ME", "NBTR.ME", "MOTZ.ME", "RDRB.ME", "PRIM.ME", "KZOS.ME", "MTLRP.ME", "GCHE.ME", "MUGSP.ME", "GTRK.ME", "IGST.ME", "PRTK.ME", "SIBN.ME", "CHEP.ME", "OBUV.ME", "OFCB.ME", "VZRZ.ME", "ALBK.ME", "SNGS.ME", "FESH.ME", "KBSB.ME", "KBTK.ME", "KMAZ.ME", "RUGR.ME", "UCSS.ME", "GTLC.ME", "SNGSP.ME", "VGSBP.ME", "VJGZP.ME", "BLNG.ME", "BSPB.ME", "DGBZ.ME", "ENRU.ME", "LSNGP.ME", "MMBM.ME", "NKNCP.ME", "TRNFP.ME", "YRSL.ME", "BISV.ME", "RASP.ME", "ROSB.ME", "ROSN.ME", "VDSB.ME", "SAGO.ME", "VSMO.ME", "RGSS.ME", "ASSB.ME", "PRIN.ME", "CHZN.ME", "SAGOP.ME", "SELGP.ME", "CNTL.ME", "TASBP.ME", "URFD.ME", "UTII.ME", "VRSBP.ME", "VTGK.ME", "VSYDP.ME", "MRKV.ME", "CBOM.ME", "ODVA.ME", "JNOS.ME", "KUZB.ME", "RUAL.ME", "AESL.ME", "DZRD.ME", "ROST.ME", "KCHE.ME", "AVAZ.ME", "MGNT.ME", "MGVM.ME", "MRKY.ME", "BGDE.ME", "IRKT.ME", "PHST.ME", "DVEC.ME", "URKA.ME", "CHMF.ME", "PSBR.ME", "GRNT.ME", "DALM.ME", "MOEX.ME", "DIXY.ME", "MRKK.ME", "MRSB.ME", "LKOH.ME", "ZILL.ME", "AFLT.ME", "GAZP.ME", "VSYD.ME", "IMOEX.ME", "FORTP.ME", "MAGN.ME", "GAZAP.ME", "RODNP.ME", "IRGZ.ME", "GRAZ.ME", "LNZL.ME", "GTPR.ME", "LPSB.ME", "LSRG.ME", "SAREP.ME", "IGSTP.ME", "SELL.ME", "KGKC.ME", "NLMK.ME", "VTBR.ME", "NKHP.ME", "MFON.ME", "KROTP.ME", "KRSG.ME", "KROT.ME", "AVAZP.ME", "MFGSP.ME", "MGTSP.ME", "SKYC.ME", "TAER.ME", "MUGS.ME", "NNSBP.ME", "TORS.ME", "MRKC.ME", "MRKU.ME", "MRKZ.ME", "OMZZP.ME", "OSMP.ME"][index] || "AAPL"
end

def full_names(index = 0)
  ["Ros Agro PLC", "Chelyabenergosbyt PAO", "N/A", "MOSKOVSKIY OBLASTNOY BANK PAO", "NPO Nauka OAO", "Nauka-Svyaz' PAO", "RBK PAO", "Seligdar PAO", "Yandex NV", "UK Arsagera PAO", "Gazprom Gazorsprdlny Rstv-Na-Donu PAO(P)", "N/A", "Investitsionnay kmpny IK Russ-Invt PAO", "Ob''yedinennaya Avistritl'ny Krprtsy PAO", "Kombinat Yuzhuralnikel' PAO", "Abrau-Durso PAO", "ANK Bashneft' PAO", "KuybyshevAzot PAO", "N/A", "Magadanenergo PAO", "MOESK PAO", "Plazmek PAO", "GK Rollman PAO", "N/A", "Tambovskaya Energosbytovaya Komp PAO (P)", "Russkaya akvakul'tura PAO", "N/A", "N/A", "Levenguk OAO", "Slavneft'-Megionneftegaz OAO", "Solikamskiy magniyevyi zavod OAO", "NEFAZ PAO", "Alrosa Nyurba PAO", "TNS energo Mariy El PAO", "Mul'tisistema OAO", "Novatek PAO", "PhosAgro PAO", "Rossiyskiye Seti PAO", "Ruspolimet PAO", "Tatneft' PAO", "Gaz PAO", "Gazkon OAO", "Federal Hydro-Generating Co RusHydro PAO", "Krasnoyarskenergosbyt PAO", "Moskovskaya Gorodskaya Telefony Set' PAO", "MRSK Tsentra i Privolzh'ya PAO", "MOSENERGO PAO", "N/A", "Yakutskenergo PAO", "Elektrotsink OAO", "Inter RAO YEES PAO", "MRSK Sibiri PAO", "Lenta Ltd", "TransContainer PAO", "AFK Sistema PAO", "N/A", "Ryazanskaya Energtchsky Sbytvy Kmpay PAO", "Var'yeganneftegaz PAO", "YATEK OAO", "Kostromskaya Sbytovaya Kompaniya PAO (P)", "Bank UralSib PAO", "RN-Zapadnaya Sibir' PAO", "N/A", "Qiwi PLC", "Volgogradenergosbyt PAO (P)", "Nizhnekamskshina PAO", "Pavlovskiy Avtobus PAO", "Sberbank Rossii PAO", "GMK Noril'skiy nikel' PAO", "Dagestanskaya energosbytovay komp PAO(P)", "Mobil'nye Telesistemy PAO", "Nizhnekamskneftekhim PAO", "Sollers PAO", "AKB Primor'ye PAO (P)", "Ural'skaya kuznitsa PAO", "TNS Energo Nizhniy Novgorod PAO", "OGK-2 PAO", "FSK YeES PAO", "Tantal OAO", "GK TNS energo PAO", "N/A", "Ingrad PAO", "Polyus PAO", "NPK OVK PAO", "N/A", "NK Russneft' PAO", "N/A", "Meridian PAO", "Taganrogskiy kombaynovyi zavod OAO (P)", "Sibirskiy Gostinets PAO", "GALS-Development PAO", "Chelyabinskiy kuznechno-prssvy zavod PAO", "Tuymazinskiy Zavod Avtobetonovozov PAO", "Zvezda PAO (P)", "Akron PAO", "Mostotrest PAO", "Tuchkovskiy Kombinat Strtlnykh Mtrlv PAO", "Novorossiyskiy morskoy torgovyi port PAO", "UK Yuzhnyi Kuzbass PAO", "Farmsintez PAO", "Rusolovo PAO", "Lenenergo PAO", "Vladimirskiy Khimicheskiy Zavod PAO", "Korshunovskiy GOK PAO", "Kaluzhskaya Sbytovaya Kompaniya PAO", "Kuban'energo PAO", "Chelyabinskiy Metallurgicheskiy Kmbt PAO", "Diod PAO", "SZP PAO", "Mechel PAO", "Institut Stvolovykh Kletok Cheloveka PAO", "Permskaya Energosbytovaya Kompaniya PAO", "N/A", "Energosbyt Rostovenergo OAO", "N/A", "Raketno-kosmchsky krp En imn SP Krl PAO", "TNS energo Yaroslavl PAO", "M.video PAO", "GAZ Tek OAO", "Tattelekom PAO", "Rostelekom PAO", "N/A", "N/A", "N/A", "N/A", "N/A", "N/A", "Detskiy Mir PAO", "Gruppa Kompaniy PIK PAO", "Khimprom PAO", "Polymetal International PLC", "ChZPSN-Profnastil PAO", "N/A", "Aviakompaniya UTair PAO", "AK Alrosa PAO", "N/A", "N/A", "N/A", "Aptechnaya set' 36,6'' PAO", "FG Budushcheye PAO", "TNS Energo Voronezh PAO", "N/A", "Yedinye Tekhno Sistemy PAO", "Krasnyi kotel'shchik TKZ OAO", "N/A", "N/A", "N/A", "N/A", "AKB Avangard PAO", "Trubnaya Metallurgicheskaya Kmpny PAO", "Saratovenergo PAO", "N/A", "Tsentr mezhdunarodnoy torgovli PAO", "N/A", "N/A", "Armada PAO", "Vtorresursy PAO", "Stavropol'energosbyt PAO", "Saratovskiy NPZ PAO", "N/A", "Buryatzoloto PAO", "N/A", "N/A", "Morion PAO", "N/A", "En+ Group PLC", "N/A", "Invest-Development PAO", "N/A", "N/A", "N/A", "N/A", "Gaz-servis OAO", "N/A", "N/A", "N/A", "Rossiyskiy Aktsnrny Kmrhky Drzhy Bnk PAO", "N/A", "Organicheskiy Sintez KPAO", "N/A", "Gruppa Cherkizovo PAO", "N/A", "Globaltruck Management PAO", "Izhstal' PAO (P)", "Protek PAO", "Gazprom Neft' PAO", "Chelyabinskiy Truboprokatnyi Zavod PAO", "OR PAO", "N/A", "Vozrozhdenie Bank", "Best Efforts Bank PAO", "Surgutneftegaz PAO", "Dal'nevostochnoye Morskoye Parkhdstv PAO", "TNS Energo Kuban' PAO", "Kuzbasskaya Toplivnaya Kompaniya PAO", "Kamaz PAO", "Rusgrain Holding PAO", "Ob''yedinennye Kreditnye Sistemy PAO", "GTL OAO", "N/A", "N/A", "N/A", "Belon OAO", "Bank Sankt-Peterburg PAO", "N/A", "Enel Rossiya PAO", "N/A", "N/A", "N/A", "N/A", "N/A", "Bashinformsvyaz' PAO (P)", "Raspadskaya PAO", "Rosbank PAO", "NK Rosneft' PAO", "Vladimirskaya Energosbytovaya Kom PAO", "Samaraenergo PAO (P)", "Korporatsiya Vsmpo-Avisma PAO", "SK Rosgosstrakh PAO", "Astrakhanskaya energosbytvy komp PAO (P)", "N/A", "N/A", "N/A", "N/A", "Tsentral'nyi Telegraf PAO", "N/A", "N/A", "N/A", "N/A", "N/A", "N/A", "MRSK Volgi PAO", "Moskovskiy Kreditnyi Bank PAO", "Mediaholding PAO", "Slavneft'-Yaroslavnefteorgsintez OAO", "Bank Kuznetskiy PAO", "United Company Rusal Plc", "N/A", "Donskoy plant of radio components OJSC", "Rosinter Restorants Holding PAO", "Kamchatskenergo PAO", "Avtovaz PAO", "Magnit PAO", "Media gruppa Voyna i Mir OAO", "MRSK Yuga PAO", "N/A", "NP korporatsiya Irkut PAO", "N/A", "Dal'nevostochnaya Energtchskya Kmpny PAO", "Uralkaliy PAO", "Severstal' PAO", "N/A", "Gorodskiye Innovatsionnye TekhnologiiPAO", "N/A", "Moskovskaya Birzha MMVB-RTS PAO", "N/A", "MRSK Severnogo Kavkaza PAO", "Mordovskaya energosbytovaya kompaniyaPAO", "NK Lukoil PAO", "AMO ZIL PAO", "Aeroflot-Rossiyskiye Avialinii PAO", "Gazprom PAO", "Vyborgskiy Sudostroitel'nyi Zavod PAO", "MOEX Russia Index", "N/A", "Magnitogorskiy Mtallurgchsky KmbntPAO", "N/A", "N/A", "Irkutskenergo PAO", "N/A", "Lenozoloto PAO", "GlavTorgProdukt", "Lipetskaya energosbytovaya komp. OAO", "Gruppa LSR PAO", "N/A", "N/A", "N/A", "Kurganskaya Generiruyushchaya Kompny PAO", "Novolipetsk Steel PAO", "Bank VTB PAO", "Novorossiyskiy Kombinat Khlebprodktv PAO", "Megafon PAO", "N/A", "N/A", "MKF Krasnyi Oktyabr' PAO", "N/A", "N/A", "N/A", "N/A", "N/A", "N/A", "N/A", "Tomskaya Raspredelitel'ny Kompny PAO (P)", "MRSK Tsentra PAO", "MRSK Urala OAO", "MRSK Severo-Zapada PAO", "N/A", "N/A"][index] || "Ros Agro PLC"
end

def lifetime_summary_json
  {
    'years' => {
      '2012' => {
        'result' => { 'percent' => 33.64, 'absolute' => 3.48 },
        'monthes' => {
          '11' => { 'percent' => 34.95, 'absolute' => 3.67 },
          '12' => { 'percent' => -1.31, 'absolute' => -0.19 }
        }
      },
      '2013' => {
        'result' => { 'percent' => 148.73, 'absolute' => 32.81 },
        'monthes' => {
          '1' => { 'percent' => -4.62, 'absolute' => -0.67 },
          '2' => { 'percent' => 31.29, 'absolute' => 4.38 },
          '3' => { 'percent' => -8.5, 'absolute' => -1.57 },
          '4' => { 'percent' => 3.44, 'absolute' => 0.58 },
          '5' => { 'percent' => 63.77, 'absolute' => 11.16 },
          '6' => { 'percent' => -7.16, 'absolute' => -2.07 },
          '7' => { 'percent' => 58.93, 'absolute' => 16.2 },
          '8' => { 'percent' => -10.15, 'absolute' => -4.46 },
          '9' => { 'percent' => 15.11, 'absolute' => 6.14 },
          '10' => { 'percent' => 5.18, 'absolute' => 2.42 },
          '11' => { 'percent' => 2.29, 'absolute' => 1.13 },
          '12' => { 'percent' => -0.85, 'absolute' => -0.43 }
        }
      },
      '2014' => {
        'result' => {
          'percent' => 36.470000000000006, 'absolute' => 12.360000000000001
        },
        'monthes' => {
          '1' => { 'percent' => 26.6, 'absolute' => 13.3 },
          '2' => { 'percent' => 19.51, 'absolute' => 12.29 },
          '3' => { 'percent' => 5.06, 'absolute' => 3.68 },
          '4' => { 'percent' => -25.8, 'absolute' => -19.94 },
          '5' => { 'percent' => 11.3, 'absolute' => 6.61 },
          '6' => { 'percent' => 14.76, 'absolute' => 9.71 },
          '7' => { 'percent' => 1.07, 'absolute' => 0.82 },
          '8' => { 'percent' => 12.21, 'absolute' => 9.24 },
          '9' => { 'percent' => -12.08, 'absolute' => -10.29 },
          '10' => { 'percent' => 10.48, 'absolute' => 7.86 },
          '11' => { 'percent' => -8.39, 'absolute' => -7.0 },
          '12' => { 'percent' => -18.25, 'absolute' => -13.92 }
        }
      },
      '2015' => {
        'result' => {
          'percent' => 9.339999999999998, 'absolute' => 0.3199999999999985
        },
        'monthes' => {
          '1' => { 'percent' => 14.89, 'absolute' => 9.33 },
          '2' => { 'percent' => -26.42, 'absolute' => -18.92 },
          '3' => { 'percent' => 4.9, 'absolute' => 2.55 },
          '4' => { 'percent' => 15.86, 'absolute' => 8.7 },
          '5' => { 'percent' => 0.37, 'absolute' => 0.24 },
          '6' => { 'percent' => 7.17, 'absolute' => 4.65 },
          '7' => { 'percent' => -14.83, 'absolute' => -10.29 },
          '8' => { 'percent' => -7.24, 'absolute' => -4.26 },
          '9' => { 'percent' => 1.98, 'absolute' => 1.06 },
          '10' => { 'percent' => 3.7, 'absolute' => 2.03 },
          '11' => { 'percent' => 5.84, 'absolute' => 3.34 },
          '12' => { 'percent' => 3.12, 'absolute' => 1.89 }
        }
      },
      '2016' => {
        'result' => { 'percent' => -25.919999999999998, 'absolute' => -21.35 },
        'monthes' => {
          '1' => { 'percent' => -4.47, 'absolute' => -2.72 },
          '2' => { 'percent' => -9.84, 'absolute' => -5.68 },
          '3' => { 'percent' => 17.14, 'absolute' => 9.01 },
          '4' => { 'percent' => 2.83, 'absolute' => 1.73 },
          '5' => { 'percent' => -30.7, 'absolute' => -19.25 },
          '6' => { 'percent' => -21.51, 'absolute' => -9.28 },
          '7' => { 'percent' => 15.67, 'absolute' => 5.31 },
          '8' => { 'percent' => 31.28, 'absolute' => 12.26 },
          '9' => { 'percent' => 4.33, 'absolute' => 2.21 },
          '10' => { 'percent' => -10.73, 'absolute' => -5.78 },
          '11' => { 'percent' => -10.13, 'absolute' => -4.88 },
          '12' => { 'percent' => -9.79, 'absolute' => -4.28 }
        }
      },
      '2017' => {
        'result' => { 'percent' => 107.07, 'absolute' => 71.15 },
        'monthes' => {
          '1' => { 'percent' => 2.7, 'absolute' => 1.08 },
          '2' => { 'percent' => 7.76, 'absolute' => 3.19 },
          '3' => { 'percent' => 3.15, 'absolute' => 1.41 },
          '4' => { 'percent' => 5.93, 'absolute' => 2.74 },
          '5' => { 'percent' => 19.06, 'absolute' => 9.34 },
          '6' => { 'percent' => -0.53, 'absolute' => -0.31 },
          '7' => { 'percent' => 21.62, 'absolute' => 12.71 },
          '8' => { 'percent' => 3.86, 'absolute' => 2.78 },
          '9' => { 'percent' => 15.64, 'absolute' => 11.74 },
          '10' => { 'percent' => 2.52, 'absolute' => 2.22 },
          '11' => { 'percent' => 13.6, 'absolute' => 12.35 },
          '12' => { 'percent' => 11.76, 'absolute' => 11.9 }
        }
      },
      '2018' => {
        'result' => { 'percent' => -53.49999999999999, 'absolute' => -52.16 },
        'monthes' => {
          '1' => { 'percent' => 16.48, 'absolute' => 18.81 },
          '2' => { 'percent' => -0.59, 'absolute' => -0.77 },
          '3' => { 'percent' => -18.63, 'absolute' => -24.08 },
          '4' => { 'percent' => -6.42, 'absolute' => -6.61 },
          '5' => { 'percent' => 20.85, 'absolute' => 19.91 },
          '6' => { 'percent' => -14.43, 'absolute' => -16.94 },
          '7' => { 'percent' => -5.35, 'absolute' => -5.27 },
          '8' => { 'percent' => -17.39, 'absolute' => -16.09 },
          '9' => { 'percent' => -1.98, 'absolute' => -1.51 },
          '10' => { 'percent' => -14.89, 'absolute' => -11.18 },
          '11' => { 'percent' => 5.57, 'absolute' => 3.59 },
          '12' => { 'percent' => -16.72, 'absolute' => -12.02 }
        }
      },
      '2019' => {
        'result' => {
          'percent' => 8.849999999999998, 'absolute' => 1.0099999999999993
        },
        'monthes' => {
          '1' => { 'percent' => 17.66, 'absolute' => 10.42 },
          '2' => { 'percent' => 1.6, 'absolute' => 1.11 },
          '3' => { 'percent' => 17.99, 'absolute' => 12.81 },
          '4' => { 'percent' => -1.04, 'absolute' => -0.89 },
          '5' => { 'percent' => -19.71, 'absolute' => -16.8 },
          '6' => { 'percent' => 2.47, 'absolute' => 1.68 },
          '7' => { 'percent' => -10.12, 'absolute' => -7.32 }
        }
      }
    },
    'lowest' => 64.72,
    'highest' => 142.97,
    'total_result' => 54.49
  }
end
