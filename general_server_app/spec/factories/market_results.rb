FactoryBot.define do
  factory :market_result do
    market
    stock_symbol
    year { rand(1972..2020) }
    month { rand(1..12) }
    percent_result { rand(-5..5) }
    absolute_result { rand(-5..5) }
    category { [:loosers, :leaders][rand(0..1)] }

    trait(:loosers) { category { :loosers } }
    trait(:leaders) { category { :leaders } }
  end
end
