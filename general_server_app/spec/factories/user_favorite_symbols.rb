# frozen_string_literal: true

FactoryBot.define do
  factory :user_favorite_symbol do
    user { nil }
    stock_symbol { nil }
  end
end
