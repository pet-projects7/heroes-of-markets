# frozen_string_literal: true

require 'rails_helper'

RSpec.describe MarketsController, type: :controller do
  render_views

  describe 'GET index' do
    it 'renders the index template' do
      get :index
      expect(response).to render_template(:index)
    end
  end

  describe 'GET show' do
    it 'renders the show template' do
      market = create(:market)

      get :show, params: { id: market }

      expect(response).to render_template(:show)
    end
  end
end
