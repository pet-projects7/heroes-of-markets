# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Api::V1::IntradayStockMarketDataController, type: :controller do
  let(:stock) { build(:stock_symbol, title: 'AAPL') }

  describe '.show', :vcr do
    context 'when correct params' do
      it 'renders json with params' do
        get :show, params: { symbol: stock.title, format: :json }

        results = JSON.parse(response.body)
        expect(results.keys).to eq %w[
          symbol
          stock_exchange_short
          timezone_name
          intraday
        ]
      end
    end

    context 'when incorrect params' do
      it 'renders error without passed symbol' do
        get :show, params: { format: :json }

        results = JSON.parse(response.body)
        expect(results.keys).to eq %w[errors]
      end

      it 'renders error when passed symbol is empty string' do
        get :show, params: { symbol: '', format: :json }

        results = JSON.parse(response.body)
        expect(results.keys).to eq %w[errors]
      end
    end
  end
end
