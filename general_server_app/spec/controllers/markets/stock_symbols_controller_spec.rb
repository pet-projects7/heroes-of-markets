# frozen_string_literal: true

RSpec.describe Markets::StockSymbolsController, type: :controller do
  let(:stock_symbol) { create(:stock_symbol) }
  let(:market) { create(:market) }

  render_views

  describe 'GET show' do
    it 'redirects when for stock symbol used id, not slug' do
      get :show,
          params: { market_id: stock_symbol.market.id, id: stock_symbol.id }

      expect(response.status).to eq(301)
    end

    it 'renders the show template' do
      get :show,
          params: { market_id: stock_symbol.market.id, id: stock_symbol.slug }

      expect(response).to render_template(:show)
    end

    it 'responses with 404 when stock symbol from incorrect market' do
      expect do
        get :show, params: { market_id: market.id, id: stock_symbol.slug }
      end.to raise_error ActiveRecord::RecordNotFound
    end
  end
end
