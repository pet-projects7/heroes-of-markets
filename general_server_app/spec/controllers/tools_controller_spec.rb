# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ToolsController, type: :controller do
  render_views

  describe '.index' do
    it 'renders index template' do
      get :index

      expect(response).to render_template(:index)
      expect(response.body).to have_content 'Covariance'
    end
  end

  describe '.covariance' do
    it 'renders covariance template' do
      get :covariance

      expect(response).to render_template(:covariance)
    end
  end
end
