# frozen_string_literal: true

require 'rails_helper'

RSpec.describe StockSymbolsController, type: :controller do
  let(:stock_symbol) { create(:stock_symbol) }

  render_views

  describe 'GET index' do
    it 'renders the index template' do
      get :index, params: { market_id: stock_symbol.market.id }

      expect(response).to render_template(:index)
    end
  end

  describe 'GET search' do
    it 'renders the search json' do
      get :search, params: { market_id: stock_symbol.market.id }, format: :json

      results = JSON.parse(JSON.parse(response.body)['data'])
      expect(results.count).to equal(1)
    end
  end
end
