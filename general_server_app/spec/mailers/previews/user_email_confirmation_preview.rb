# frozen_string_literal: true

# Preview all emails at http://localhost:3000/rails/mailers/user_email_confirmation
class UserEmailConfirmationPreview < ActionMailer::Preview; end
