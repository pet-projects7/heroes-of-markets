# frozen_string_literal: true

require 'rails_helper'

RSpec.describe UserEmailConfirmationMailer, type: :mailer do
  describe '.new' do
    let(:user) { create(:user) }
    # let(:confirmation_token) { 'https://heroes-of-markets.com/users/confirm?token=eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyIjoidXNlckBtYWlsLnJ1IiwiZXhwIjoxNTU4MjkwNDg3fQ.GSYd5Bp1YPaDhDXzLWpiPLul4EfOkmQ1edH8ImwkxWI' }

    it 'creates email' do
      described_class.confirm(user).deliver
      expect(ActionMailer::Base.deliveries.empty?).to be(false)
    end

    it 'email contains token' do
      email = described_class.confirm(user)

      token = JwtService.encode(user: user.email, exp: 1.day.from_now.to_i)
      confirmation_link =
        confirm_users_url(
          token: token, host: 'heroes-of-markets.com', protocol: 'https'
        )

      expect(email.body.raw_source).to include(confirmation_link)
    end
  end
end
