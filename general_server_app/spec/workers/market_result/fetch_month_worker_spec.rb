require 'rails_helper'

RSpec.describe MarketResult::FetchMonthWorker, type: :worker do
  let(:market) { create(:market) }
  let!(:stock_symbol) { create(:stock_symbol, market: market) }

  describe 'fetch results' do
    it 'create results in database' do
      MarketResult::FetchMonthWorker.new.perform(market.id)

      expect(MarketResult.count).to eq 162

      expect(market.market_results.reload.where(year: 2013).first.absolute_result).to eq -0.67
      expect(market.market_results.reload.where(year: 2013).first.percent_result).to eq -4.62
    end
  end
end
