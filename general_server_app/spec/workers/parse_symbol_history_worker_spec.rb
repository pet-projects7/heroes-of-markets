# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ParseSymbolHistoryWorker, :vcr, type: :worker do
  let(:stock) { create(:stock_symbol, title: 'RSTI.ME') }

  describe '.perform' do
    it 'get history info about stock' do
      expect(call_worker.response.count).to eq(1_510)
    end
  end

  private

  def call_worker
    ParseSymbolHistoryWorker.new.perform(options)
  end

  def options
    {
      symbol: stock.title,
      date_from: '1990-01-01'
    }
  end
end
