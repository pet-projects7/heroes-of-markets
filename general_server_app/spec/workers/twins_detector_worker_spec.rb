# frozen_string_literal: true

require 'rails_helper'

RSpec.describe TwinsDetectorWorker, type: :worker do
  describe 'fetch parent' do
    let!(:stock) { create(:stock_symbol, title: 'RSTI') }
    let!(:stock2) { create(:stock_symbol, title: 'RSTI.ME') }

    it 'adds parent' do
      expect { described_class.new.perform }.to change {
        stock2.reload.parent_stock
      }.from(nil)
        .to(stock)
    end
  end

  describe 'fetch children' do
    let!(:stock) { create(:stock_symbol, title: 'RSTI') }
    let!(:stock2) { create(:stock_symbol, title: 'RSTI.ME') }

    it 'adds children', :vcr do
      expect { described_class.new.perform }.to change {
        stock.children.count
      }.from(0).to(1)
    end
  end
end
