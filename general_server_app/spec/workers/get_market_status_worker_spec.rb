# frozen_string_literal: true

require 'rails_helper'

RSpec.describe GetMarketStatusWorker, type: :worker do
  let!(:market) do
    create(
      :market,
      title: 'Istanbul Stock Exchange', symbol: 'BIST', state: :active
    )
  end
  let!(:stock) do
    create(
      :stock_symbol,
      title: 'ZOREN.IS', lifetime_summary: nil, market: market
    )
  end

  describe '.perform', :vcr do
    it 'toggle market to close' do
      described_class.new.perform
      market.reload

      expect(market.working_status).to eq('closed')
    end
  end
end
