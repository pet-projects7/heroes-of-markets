# frozen_string_literal: true

require 'rails_helper'

RSpec.describe FetchStockSummaryWorker, type: :worker do
  describe '.perform', :vcr do
    let(:stock) { create(:stock_symbol, title: 'AMZN', lifetime_summary: nil) }

    it 'updates the stock summary' do
      expect(stock.lifetime_summary).to be nil
      described_class.new.perform(stock.id)
      expect(stock.reload.lifetime_summary).not_to be nil
    end

    it 'summary have info about, years, highest results' do
      described_class.new.perform(stock.id)

      keys_in_summary = %w[last first years lowest highest total_result]
      expect(stock.reload.lifetime_summary.keys).to eq(keys_in_summary)

      keys_in_summary.each do |key|
        expect(stock.lifetime_summary[key]).to be_present
      end
    end
  end
end
