# frozen_string_literal: true

require 'rails_helper'

describe 'Testing stock symbol page', :vcr, type: :feature do
  let!(:market) { create(:market) }
  let!(:stock) { create(:stock_symbol, market: market) }
  let!(:stock_duplicate) do
    create(:stock_symbol, title: 'YNDX.ME', market: market, parent_stock: stock)
  end

  describe 'index stock symbols page' do
    it 'list of all stock symbols', js: true do
      visit stock_symbols_path

      expect(page).to have_content stock.title
      expect(page).to have_content stock.full_name
    end
  end

  describe 'personal stock symbol page' do
    it 'renders title of the stock', js: true do
      visit market_stock_symbol_path(market.slug, stock.slug)

      expect(page.body).to have_content stock.title
    end

    # scenario 'renders siblings of the stocks' do
    #   visit market_stock_symbol_path(market.slug, stock.slug)
    #
    #   expect(page.body).to have_content 'There is 1 siblings:'
    # end

    it 'renders short summary' do
      visit market_stock_symbol_path(market.slug, stock.slug)

      expect(page.body).to have_css 'div', text: '142.97'
      expect(page.body).to have_css 'div', text: 'Highest price'
    end

    context 'when articles exist' do
      it "doesn't renders 4 article on page" do
      end
    end

    context "when articles doesn't exist" do
      it "doesn't renders a block " do
      end
    end
  end
end
