# frozen_string_literal: true

require 'rails_helper'

describe 'Toggle menu' do
  include Capybara::Screenshot::Diff

  # context 'when authorized user' do
  #   scenario 'can toggle menu and see links', js: true do
  #     # Capybara.current_session.current_window.resize_to(320, 640)
  #     visit root_path
  #
  #     find('.navbar-burger.burger').click
  #     expect(page.has_css?('.navbar-burger.burger.is-active', visible: true)).to be true
  #     screenshot 'toggled_menu'
  #   end
  # end
  #
  # context 'when anonymous user' do
  #   scenario 'can toggle menu and see links', js: true do
  #     visit root_path
  #
  #     find('.navbar-burger.burger').click
  #     expect(page.has_css?('.navbar-burger.burger.is-active', visible: true)).to be true
  #     screenshot 'toggled_menu'
  #   end
  # end
end
