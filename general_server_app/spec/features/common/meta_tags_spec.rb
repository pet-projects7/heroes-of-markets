# frozen_string_literal: true

require 'rails_helper'

describe 'Meta tags', type: :feature do
  let!(:market) { create(:market) }
  let!(:stock_symbol) do
    create(:stock_symbol, title: 'ABEO', full_name: 'Abeona Therapeutics Inc.', market: market)
  end

  describe 'pages', :vcr do
    it 'list of markets on the main page', js: true do
      visit markets_path

      expect(page.title).to eq('Heroes of Markets | HOM')
    end

    it 'page with one market', js: true do
      visit market_path(market.slug)

      expect(page.title).to eq("Stocks exchange market: #{ market.title } | HOM")
    end

    # scenario 'page with one stock', js: true do
    #   visit market_stock_symbol_path(market.slug, stock_symbol.slug)
    #
    #   expect(page.title).to eq('Stock symbol Abeona Therapeutics Inc. (ABEO) on stock exchange | HOM')
    # end
  end
end
