# frozen_string_literal: true

require 'rails_helper'

describe 'Favorites stocks', vcr: true, type: :feature do
  let(:user) { create(:user, confirmed: true) }
  let(:stock_symbol) { create(:stock_symbol, title: 'ALNU.ME') }

  context 'when anonymous user' do
    it "can't see a button add to stocks", js: true do
      visit market_stock_symbol_path(stock_symbol.market, stock_symbol)

      expect(page.body).not_to have_content 'Add to favorited'
    end
  end

  context 'when authenticated user' do
    it 'can add stock to favorites', authorized: true, js: true do
      visit market_stock_symbol_path(stock_symbol.market, stock_symbol)

      expect(page.body).to have_content 'Add to favorited'
    end

    # rubocop:disable RSpec/MultipleExpectations
    it 'can remove stock from favorites', authorized: true, js: true do
      visit market_stock_symbol_path(stock_symbol.market, stock_symbol)

      expect(page.body).to have_content 'Add to favorited'
      click_button 'Add to favorited'
      sleep 2
      expect(page.body).to have_content 'Remove from favorited'
    end
    # rubocop:enable RSpec/MultipleExpectations
  end
end
