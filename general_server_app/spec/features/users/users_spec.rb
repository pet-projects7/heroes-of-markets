# frozen_string_literal: true

require 'rails_helper'

describe 'Sign up new user', type: :feature do
  let(:user) { build(:user) }

  it 'valid data', :vcr, js: true do
    visit new_users_path
    fill_in 'Email', with: user.email
    fill_in 'Password', with: user.password
    click_button 'Send'
    expect(
      page.body
    ).to have_content 'Thanks, your email registered. Please confirm it.'
  end

  it 'invalid data', :vcr, js: true do
    visit new_users_path
    fill_in 'Email', with: ''
    fill_in 'Password', with: user.password
    click_button 'Send'

    expect(page.body).to have_content "Email can't be blank"
  end
end

describe 'Sign in user', type: :feature do
  let(:user) do
    user = build(:user)
    user.save
    user
  end

  it 'user not confirmed', :vcr, js: true do
    visit login_users_path

    fill_in 'Email', with: user.email
    fill_in 'Password', with: user.password
    click_button 'Send'

    expect(page.body).to have_content 'Your email not confirmed'
  end

  it 'user confirmed', :vcr, js: true do
    visit login_users_path

    user.update(confirmed: true)
    user.save(validate: false)

    authenticate_user
  end
end

describe 'Confirm user', type: :feature do
  let(:user) { build(:user) }

  it 'confirm user with valid token', :vcr, js: true do
    user.save
    @token = JwtService.encode(user: user.email, exp: 1.day.from_now.to_i)
    visit confirm_users_path(token: @token)

    user.reload
    expect(user.confirmed).to be true
    expect(page.body).to have_content I18n.t('confirm_user_command.success')
  end

  it 'confirm user with invalid token', :vcr, js: true do
    @token =
      JwtService.encode(user: "maksim#{user.email}", exp: 1.day.from_now.to_i)
    visit confirm_users_path(token: @token)

    expect(page.body).to have_content I18n.t('confirm_user_command.wrong_token')
  end

  # scenario 'confirm user with expired token', js: true do
  #   @token = JwtService.encode({ user: user.email, exp: 1.seconds.from_now.to_i})
  #   Timecop.freeze(Date.today + 30) do
  #     visit confirm_users_path(token: @token)
  #
  #     expect(page.body).to have_content I18n.t('confirm_user_command.expired')
  #   end
  # end
end
