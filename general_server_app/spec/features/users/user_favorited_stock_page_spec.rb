# # frozen_string_literal: true
#
# require 'rails_helper'
#
# describe 'Page with favorited stocks', type: :feature do
#   let(:user) { create(:user, confirmed: true) }
#   let(:stock_symbols) { create_list(:stock_symbol, 1) }
#
#   context 'when user authenticated', :vcr do
#     it 'can see list of favorited stocks', :vcr, js: true do
#       authenticate_user
#       favorite_stock_by_user
#       visit favorited_stocks_users_path
#       expect(page).to have_content(stock_symbols.first.full_name)
#     end
#   end
#
#   private
#
#   def favorite_stock_by_user
#     user.user_favorite_symbols.create stock_symbol_id: stock_symbols.first.id
#   end
# end
