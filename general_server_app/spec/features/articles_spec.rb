# frozen_string_literal: true
# 1. Должен иметь возможность увидеть список новостей в рамках одного рынка
# 2. Должен имеить возможность увидеть список новостей в рамках одной акции
# 3. Должен иметь возможность увидеть у новости на какие акции она влияет и эффект (позитивный, негативный, нейтральный)
# 4. Если новостей нет, то блока "Новости" у акции нет.
# 6. У новостей есть микроразметка

# <div itemscope itemtype="http://schema.org/Article">
#   <span itemprop="name">How to Tie a Reef Knot</span>
#   by <span itemprop="author">John Doe</span>
#   This article has been tweeted 1203 times and contains 78 user comments.
#   <div itemprop="interactionStatistic" itemscope itemtype="http://schema.org/InteractionCounter">
#     <div itemprop="interactionService" itemscope itemid="http://www.twitter.com" itemtype="http://schema.org/WebSite">
#       <meta itemprop="name" content="Twitter" />
#     </div>
#     <meta itemprop="interactionType" content="http://schema.org/ShareAction"/>
#     <meta itemprop="userInteractionCount" content="1203" />
#   </div>
#   <div itemprop="interactionStatistic" itemscope itemtype="http://schema.org/InteractionCounter">
#     <meta itemprop="interactionType" content="http://schema.org/CommentAction"/>
#     <meta itemprop="userInteractionCount" content="78" />
#   </div>
# </div>
# frozen_string_literal: true

# require 'rails_helper'

# describe 'List of articles on the one market' do
#   context "when articles exist" do
#     it 'renders 4 articles' do
#       pending
#     end

#     it 'renders related stocks and effect on them' do
#       pending
#     end
#   end

#   context "when articles doesn't exist" do
#     it 'renders text with describing' do
#       pending
#     end
#   end
# end
