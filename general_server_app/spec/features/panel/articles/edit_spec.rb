# frozen_string_literal: true
# require 'rails_helper'
#
# describe 'Edit on article' do
#  let!(:articles) { create_list(:article, 23) }
#
#  context 'when admin' do
#    let(:user) { create(:panel_user) }
#    before { sign_in(user) }
#
#    it 'renders list of news for edit' do
#      visit panel_articles_path
#      expect(page).to have_content articles.first.title
#    end
#  end
# end
