# frozen_string_literal: true

require 'rails_helper'

describe 'Create an article', js: true do
  let(:user) { create(:panel_user) }

  before { sign_in(user) }

  it 'leads to form for article creating' do
    visit panel_articles_path
    click_link 'Create new'
    expect(page).to have_content 'Create new'
  end

  context 'when valid parameters for article' do
    it 'creates an article' do
      fill_article_form
    end
  end

  context 'when invalid parameters for article' do
  end

  private

  def fill_article_form
    visit new_panel_article_path
    fill_in 'Title', with: Faker::Coffee.blend_name

    # Т.к. методы типа fill или send_keys не работают для поля
    # которое на самом деле не textarea, а div[contenteditable]
    execute_script("document.querySelector('#article_content').value = '#{Faker::Lorem.paragraph}'")

    fill_in 'Source description', with: Faker::Lorem.word
    fill_in 'Source link', with: Faker::Lorem.word

    click_button 'Create'
    expect(page.body).to have_content('Effect on symbols')
  end
end
