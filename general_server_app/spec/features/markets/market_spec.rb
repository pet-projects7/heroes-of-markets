# frozen_string_literal: true

require 'rails_helper'

feature 'Market' do
  let(:market) { create(:market, :active) }
  let(:market_2) { create(:market, :active) }
  let(:stock_symbol) { create(:stock_symbol, market: market) }
  let(:market_leaders) { [
      create_result_object(:leaders, market, stock_symbol, 1),
      create_result_object(:leaders, market, stock_symbol, 2)
  ] }
  let(:market_loosers) { [
      create_result_object(:loosers, market, stock_symbol, 1),
      create_result_object(:loosers, market, stock_symbol, 2)
  ] }
  let!(:stocks) { create_list(:stock_symbol, 20, market: market) }

  context "when market exist", js: true do
    it 'should render stocks from this market' do
      visit market_path(market)
      expect(page).to have_content stocks.first.title
    end

    it 'should render results of the current year' do
      expect(market_leaders.count).to eq 2
      expect(market_loosers.count).to eq 2

      visit market_path(market)
      current_year = DateTime.now.year
      expect(page).to have_content "Leaders of #{ current_year } year:"
      expect(page).to have_content market_leaders.first.stock_symbol.title
    end

    #it 'should render results of previous year and link to all years' do
    #  visit market_path(market)
    #  expect(page).to have_content 'Leaders of previous year:'
    #end
    #
    #it 'should render the date of opening' do
    #  visit market_path(market)
    #  expect(page).to have_content 'First time opened at 12 february 2012'
    #end

    context 'when results doesn\'t exist' do
      it 'should not render info about leaders' do
        visit market_path(market_2)
        expect(page).not_to have_content "Leaders"
      end
    end
  end

  context 'when market doesn\'t exist' do
    it 'should give 404 response' do
      expect {
        visit market_path(111)
      }.to raise_error(ActiveRecord::RecordNotFound)
    end
  end

  def create_result_object(category, market, stock_symbol, month)
    create(:market_result,
          category: category,
          market: market,
          year: 2020,
          month: month,
          stock_symbol: stock_symbol)
  end
end
