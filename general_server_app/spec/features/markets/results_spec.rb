# frozen_string_literal: true

require 'rails_helper'

feature 'Market results', js: true do
  let!(:market) { create(:market, :active) }
  let!(:stock_symbol) { create(:stock_symbol, market: market) }
  let!(:market_result) { create(:market_result,
         category: :leaders,
         market: market,
         year: 2020,
         month: 0,
         stock_symbol: stock_symbol) }
  let!(:marketPageObject) { PageObjects::MarketPage.new(self) }

  scenario 'should allow user follow the link from market page' do
    visit market_path(market)
    marketPageObject.results_link.click
    expect(page).to have_content 'leaders and loosers'
  end

  scenario 'should render years and monthes' do
    visit market_results_path(market)
    expect(page).to have_content(market.market_results.first.year)
  end
end

module PageObjects
  class MarketPage < SimpleDelegator
    def results_link
      find_link 'Results from previous years'
    end
  end
end
