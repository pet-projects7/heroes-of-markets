# frozen_string_literal: true

require 'rails_helper'

describe 'Markets list', type: :feature do
  let!(:markets) { create_list(:market, 5, state: :active) }
  let!(:market_with_wrong_symbol) { create(:market, state: :active, symbol: 'ABRACADABRA') }
  let!(:stock) { create(:stock_symbol, market: markets.first) }

  before { visit markets_path }

  it 'user can see full list of markets on one page' do
    expect(page).to have_content markets.first.title
  end

  it 'should render stocks count for market' do
    expect(markets.first.stock_symbols.size).to eq 1
  end

  it 'should render the flag' do
    expect(page).to  have_selector '.itemsList__item', maximum: 6
    expect(page).to  have_selector '.itemsList__flag', maximum: 5
  end
end
