# frozen_string_literal: true

require 'rails_helper'

feature 'Covariance calculation' do
  given!(:first_stock) { create(:stock_symbol, title: 'SBER.ME') }
  given!(:second_stock) { create(:stock_symbol, title: 'NFLX') }

  scenario 'works correct', :vcr, js: true do
    visit covariance_tools_path

    expect(page).to have_button('Calc variance', disabled: true)

    fill_in 'First stock', with: first_stock.title
    sleep 2
    fill_in 'Second stock', with: second_stock.title

    expect(page).to have_button('Calc variance', disabled: false)
    click_on 'Calc variance'

    expect(
        page
    ).to have_content 'Resulting covariance is 0.5980007800440269. Used 1973 dates. First date is 2011-11-21 and last is 2020-01-27'
  end
end
