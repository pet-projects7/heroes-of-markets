# frozen_string_literal: true

require 'rails_helper'

describe 'Panel', type: :feature do
  before { sign_in(user) }

  context 'when user has role panel_users' do
    let(:user) { create(:panel_user) }

    it 'allow to sign in' do
      visit panel_root_path
      expect(page).to have_content 'Heroes of markets'
    end
  end

  context 'when user has no role panel_users' do
    let(:user) { create(:user, :confirmed) }

    it 'forbidden to sign in' do
      visit panel_root_path
      expect(page).to have_content 'You are being redirected'
    end
  end
end
