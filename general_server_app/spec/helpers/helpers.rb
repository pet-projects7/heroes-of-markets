# frozen_string_literal: true

# Хелпер для использования в feature тестах
module Helpers
  # :reek:TooManyStatements { max_statements: 9 }
  def authenticate_user
    user_email = user.email
    visit login_users_path

    user.confirmed = true
    user.save

    fill_in 'Email', with: user_email
    fill_in 'Password', with: user.password
    click_button 'Send'
    sleep 2

    expect(page.body).to have_content user_email
  end

  def logout
    cookies.signed['user_id'] = nil
  end
end
