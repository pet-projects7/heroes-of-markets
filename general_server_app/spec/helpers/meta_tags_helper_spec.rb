# frozen_string_literal: true

require 'rails_helper'

RSpec.describe MetaTagsHelper, type: :helper do
  it 'returns default meta tag for title' do
    default_meta_tags = %i[
      site
      title
      description
      keywords
      charset
      image_src
      og
      icon
      reverse
    ]
    expect(helper.heroes_of_markets_meta_tags[:title]).to eq 'Heroes of Markets'
    expect(helper.heroes_of_markets_meta_tags.keys).to eq default_meta_tags
  end

  # it 'returns default meta tag for description' do
  #   default_meta_tags = %i[
  #     site
  #     title
  #     description
  #     keywords
  #     charset
  #     image_src
  #     og
  #     icon
  #     reverse
  #     noindex
  #     nofollow
  #   ]
  #   expect(helper.stock_symbols_index.keys).to eq default_meta_tags
  #   expect(
  #     helper.heroes_of_markets_meta_tags[:description]
  #   ).to eq 'List of worldwide markets. Where you can follow last news and find all kind of charts.'
  # end
end
